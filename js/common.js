function area_select($province, $city, $area){
    $province.change(function(){
        var provinceId = $(this).val();
        if(provinceId){
            $.getJSON('/area/children/'+provinceId, function(data){
                var html = '<option value="">请选择</option>';
                
                $.each(data, function(i, e){
                    html += '<option value="'+e.id+'">'+e.name+'</option>';
                });
                $city.html(html);
            });
        }else{
            $city.html('');
        }
    });

    $city.change(function(){
        var cityId = $(this).val();
        if(cityId){
            $.getJSON('/area/children/'+cityId, function(data){
                var html = '<option value="">请选择</option>';
                
                $.each(data, function(i, e){
                    html += '<option value="'+e.id+'">'+e.name+'</option>';
                });
                $area.html(html);
            });
        }else{
            $area.html('');
        }
    });
}