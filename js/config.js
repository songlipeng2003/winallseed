
require.config({
    baseUrl: '/js/',
    paths: {
        'jquery': 'lib/jquery-1.11.1.min',
        'jquery.ui': 'lib/jquery-ui-1.10.3.min',
        'bootstrap': 'lib/bootstrap.min',
        'angular': 'lib/angular.min',
        'underscore': 'lib/underscore-min',
    },
    shim:{
        'jquery.ui': {
            exports: "$",
            deps: ['jquery']
        },
        'bootstrap': {
            exports: "$",
            deps: ['jquery']
        },
        'angular': {
            exports: 'angular',
            deps: ['jquery']
        },
    }
});