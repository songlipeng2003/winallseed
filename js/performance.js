/**
 * quanyin JS
 * performance
 * 2014-10-01
 */

(function() {
    var $$ = BASE.getEle,
            $_ = BASE.creEle,
            $D = BASE.disEle,
            $T = BASE.txtEle,
            $R = BASE.rmvEle,
            $G = BASE.getUrl,
            $JP = BASE.jsonParse,
            $JS = BASE.jsonStringify,
            $SS = BASE.setStorage,
            $GS = BASE.getStorage;

    var innerHTML = "innerHTML",
            className = "className",
            appendChild = "appendChild",
            style = "style";

    var body = document.body;
    var Item = 'quanyin/preformance';
    var History;
    var Ajaxing;
    var Page;


    function initialize() {
        window.onhashchange = function() {
            var hash = location.hash;
            if (hash === '#desc') {
                showDescPage();
            } else if (hash === '#public') {
                showPublicPage();
            } else {
                showHomePage();
            }
        };
        window.onhashchange();
        window.onscroll = onscroll;
    }

    function showHomePage() {
        var page = $$("#home");
        var searchBtn = $$("#searchBtn");
        var searchInput = $$("#searchInput");
        var publicBtn = $$("#publicBtn");

        if (!page.register) {
            initEvent();
            page.scrollEvent = scrollEvent;
            page.register = true;
            page.page = '1';
            page.pageCon = 0;
            searchBtn.onclick();
        }
        History = history.length;

        showPage('home');

        function initEvent() {
            searchBtn.onclick = searchEvent;
            searchInput.onkeyup = function(e) {
                if (e.keyCode === 13)
                    searchEvent();
            };
            publicBtn.onclick = publicEvent;
        }

        function searchEvent() {
            if (Ajaxing)
                return BASE.alert('请稍等');
            BASE.loading('1');
            var reqObj = {
                keyword: searchInput.value,
                page: page.page
            };
            BASE.ajax({
                url: '/display/list',
                data: reqObj,
                timeout: 8e3,
                method: 'GET',
                error: function() {
                    Ajaxing = false;
                    BASE.loading();
                    errorFun();
                },
                success: function(data) {
                    Ajaxing = false;
                    BASE.loading();
                    successFun($JP(data));
                }
            });
            function errorFun(info) {
                if (Page !== 'home')
                    return;
                !info && (info = "网络连接超时");
                BASE.alert(info);
            }
            function successFun(data) {
                if (!data || data.code !== '0000') {
                    var info = '获取数据失败';
                    data && data.info && (info = data.info);
                    return errorFun(info);
                }
                if (data.data.currPage === '1') {
                    if (!data.data.ary.length) {
                        return BASE.alert('没有相关记录');
                    } else {
                        cleanPage();
                        page.totalPage = data.data.totalPage;
                    }
                }

                addPictures(data.data.ary);
            }
        }
        function scrollEvent() {
            if (page.totalPage === page.page)
                return;
            page.page = page.page - 0 + 1 + '';
            searchEvent();
        }
        function publicEvent() {
            if ($G('user')) {
                BASE.alert('很抱歉，您没有发布权限');
            } else {
                location.hash = "#public";
            }
        }

        function addPictures(ary) {
            for (var i = 0; i < ary.length; i++) {
                deal(ary[i]);
            }

            function deal(obj) {
                var id = page.pageCon;
                var con = $$("#home_con" + id);
                var div = $_('', con);
                var img = $_('img', div);
                img.src = obj.img;
                var lab = $_('', div, '', 'text-ellipsis home-conLab');
                $T(lab, obj.location);
                div.onclick = function() {
                    $SS(Item, $JS(obj));
                    location.hash = "#desc";
                };

                id += 1;
                (id === 3) && (id = 0);
                page.pageCon = id;
            }
        }

        function cleanPage() {
            page.pageCon = 0;
            for (var i = 0; i < 3; i++) {
                $R($$("#home_con" + i));
            }
        }
    }
    function showDescPage() {
        var page = $$("#desc");

        if (!page.register) {
            initEvent();
            page.register = true;
        }
        initPage();
        showPage('desc');

        function initEvent() {
            $$("#desc_btn").onclick = goback;
            page.addEventListener('swiperight', goback, false);
        }
        function initPage() {
            var obj = $JP($GS(Item));
            $$("#desc_img").src = obj.img;
            $$("#desc_avatar").src = obj.avatar;
            $T($$("#desc_userid"), obj.nickname);
            $T($$("#desc_time"), obj.createTime);
            $T($$("#desc_location"), obj.location);
            $T($$("#desc_desc"), obj.description);
        }
    }
    function showPublicPage() {
        var page = $$("#public");
        var lng, lat, locName;
        if (!page.register) {
            initEvent();
            page.register = true;
        }
        getLocation();
        showPage('public');

        window.baiduLoc = function(data) {
            if (data.status) {
                locName = '';
                $T($$("#public_loc"), '未知');
                BASE.alert(data.msg);
            } else {
                locName = data.result.formatted_address;
                $T($$("#public_loc"), locName);
            }
        };
        function initEvent() {
            $$("#file").onchange = addPicture;
            $$("#public_loc").onclick = function() {
                if (!locName)
                    getLocation();
            };
            $$("#public_send").onclick = publicInfo;
            $$("#public_addPic").onclick = function() {
                $$("#file").click();
            };
            $$("#public_btn").onclick = goback;
            page.addEventListener('swiperight', goback, false);
        }
        function getLocation() {
            if (navigator.geolocation) {
                var options = {
                    timeout: 8e3
                };
                navigator.geolocation.getCurrentPosition(success, error, options);
            } else {
                error();
            }

            function success(position) {
                lng = position.coords.longitude;
                lat = position.coords.latitude;
                BASE.jsReader('http://api.map.baidu.com/geocoder/v2/?ak=CcrmBVNbvuv37TSqFGUAUq1G&output=json&callback=baiduLoc&location=' + lat + ',' + lng);
            }
            function error() {
                lng = '';
                lat = '';
                locName = '';
                $T($$("#public_loc"), '未知');
                BASE.alert('获取位置信息失败');
            }
        }
        function addPicture() {
            var fileInput = $$("#file");
            var addPic = $$("#public_addPic");
            var picNum = $$("#public_picNum");
            var files = fileInput.files;
            var num = $$(".public-conImgDiv").length;
            if (num < 3) {
                for (var i = 0; i < files.length; i++) {
                    deal(files[i]);
                }
            }
            if (num < 3) {
                $T(picNum, 3 - num);
            } else {
                $T(picNum, 0);
                $D(addPic);
            }
            fileInput.value = '';
            fileInput.blur();

            function deal(file) {
                if (num >= 3)
                    return $D(addPic);
                if (!file.type.match(/image.*/))
                    return;
                else if (file.size / 1024 / 1024 > 10)
                    return;
                num++;

                var div = $_('', '', '', 'public-conImgDiv');
                div.file = file;
                addPic.parentNode.insertBefore(div, addPic);
                var img = $_('img', div);
                var icon = $_('', div);
                $_('', icon);

                div.onclick = function() {
                    $R(div, 1);
                    var len = $$(".public-conImgDiv").length;
                    if (len < 3) {
                        $T(picNum, 3 - len);
                        $D(addPic, 'inline-block');
                    }
                };

                var reader = new FileReader();
                reader.onload = function(e) {
                    img.src = e.target.result;
                };
                reader.readAsDataURL(file);
            }
        }
        function publicInfo() {
            var imgAry = $$(".public-conImgDiv");
            if (!verify())
                return;
            BASE.loading('2');
            setTimeout(function() {
                ajaxFile(0);
            }, 0);

            function verify() {
                var ret;
                if (!imgAry.length) {
                    BASE.alert('请上传图片');
                }
                // else if (!locName) {
                //     BASE.alert('未能获取到您的位置信息');
                //     getLocation();
                // }
                else {
                    ret = true;
                }

                return ret;
            }
            function ajaxFile(i) {
                if (Ajaxing)
                    return BASE.alert('请稍等');
                BASE.alert('上传图片' + i + ':...', true);
                Ajaxing = true;
                var fileName = imgAry[i].file.name;
                var index = fileName.lastIndexOf('.');
                if (index <= 0 || index === fileName.length - 1)
                    return BASE.alert('图片不符合要求');
                var suffix = fileName.slice(index + 1);
                BASE.ajaxFile({
                    url: '/display/upload',
                    send: imgAry[i].file,
                    load: function(data) {
                        Ajaxing = false;
                        successFun($JP(data));
                    },
                    error: function() {
                        Ajaxing = false;
                        errorFun();
                    },
                    abort: function() {
                        Ajaxing = false;
                        errorFun('图片上传取消');
                    },
                    progress: function(e) {
                        BASE.alert('上传图片' + i + ': ' + e + "%", true);
                    }
                });
                function errorFun(info) {
                    !info && (info = "网络连接超时");
                    BASE.alert(info);
                    BASE.loading();
                }
                function successFun(data) {
                    if (!data || data.code !== '0000') {
                        var info = '图片上传出错';
                        data && data.info && (info = data.info);
                        return errorFun(info);
                    }
                    imgAry[i].filename = data.data;
                    if (i + 1 < imgAry.length) {
                        setTimeout(function() {
                            ajaxFile(i + 1);
                        }, 0);
                    } else {
                        setTimeout(function() {
                            ajaxInfo();
                        }, 0);
                    }
                }
            }
            function ajaxInfo() {
                if (Ajaxing)
                    return BASE.alert('请稍等');
                Ajaxing = true;
                var reqObj = {
                    description: $$("#public_desc").value, //描述
                    longitude: lng, //经度
                    latitude: lat, //纬度
                    location: locName
                };
                var ary = [];
                for (var i = 0; i < imgAry.length; i++) {
                    ary.push(imgAry[i].filename);
                }
                reqObj.pictureAry = ary;
                BASE.ajax({
                    url: '/display/create',
                    timeout: 8e3,
                    method: 'POST',
                    send: $JS(reqObj),
                    error: function() {
                        Ajaxing = false;
                        errorFun();
                    },
                    success: function(data) {
                        Ajaxing = false;
                        successFun($JP(data));
                    }
                });
                function errorFun(info) {
                    BASE.loading();
                    !info && (info = "网络连接超时");
                    BASE.alert(info);
                }
                function successFun(data) {
                    if (!data || data.code !== '0000') {
                        var info = '发送数据失败';
                        data && data.info && (info = data.info);
                        return errorFun(info);
                    }
                    BASE.loading();
                    BASE.alert('发送成功');
                    $$("#public_desc").value = '';
                    var len = imgAry.length;
                    for (var i = 0; i < len; i++) {
                        $$(".public-conImgDiv")[0].click();
                    }
                }
            }
        }
    }

    function onscroll() {
        if (Page === 'home') {
            if (body.scrollTop > body.scrollHeight - window.innerHeight - window.innerHeight / 720 * 100) {
                BASE.execFun($$("#home").scrollEvent);
            }
        }
    }

    function showPage(id) {
        BASE.loading();
        BASE.alert();
        var ary = ['home', 'desc', 'public'];
        for (var i = 0; i < ary.length; i++) {
            $$("#" + ary[i]) && $D($$("#" + ary[i]));
        }
        if (id && $$("#" + id)) {
            $D($$("#" + id), 1);
            Page = id;
        }
    }
    function goback() {
        if (History && History < history.length) {
            history.back();
        } else {
            location.hash = "#home";
        }
    }

    (function() {
       BASE.txtReader('css/public.adp.css', function(styleStr) {
           BASE.adpAllStyle(styleStr, 'public_adp', function() {
               BASE.txtReader('css/performance.adp.css', function(styleStr) {
                   BASE.adpAllStyle(styleStr, 'performance_adp', initialize);
               });
           });
       });

        BASE.txtReader('/css/performance.adp.css', function (styleStr) {
            BASE.adpAllStyle(styleStr, 'performance_adp', initialize);
        });
    })();
})();
