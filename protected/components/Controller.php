<?php
require_once(Yii::app()->basePath.'/vendor/wechat-php-sdk/wechat.class.php');

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    protected $weObj;

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public function init()
	{
		parent::init();

        $options = array(
            'token'=>Yii::app()->params['weixinToken'], //填写你设定的key
            'appid'=>Yii::app()->params['weixinAppid'], //填写高级调用功能的app id, 请在微信开发模式后台查询
            'appsecret'=>Yii::app()->params['weixinppsecret'], //填写高级调用功能的密钥
        );

        $this->weObj = new Wechat($options);
	}

	/**
	 * Return data to browser as JSON and end application.
	 * @param array $data
	 */
	protected function renderJSON($data)
	{
	    header('Content-type: application/json');
	    if(is_array($data)){
	    	echo CJSON::encode($data);
	    }
	    else
	    {
	    	echo $data;
	    }

	    foreach (Yii::app()->log->routes as $route)
	    {
	        if($route instanceof CWebLogRoute)
	            $route->enabled = false; // disable any weblogroutes

	        if($route instanceof YiiDebugToolbarRoute)
	            $route->enabled = false; // disable any weblogroutes
	    }
	    Yii::app()->end();
	}

	protected function getWeixinAccessToken()
	{
		$accessToken = Yii::app()->cache->get('weixin_access_token');
		if(!$accessToken){
	        $weObj = new Wechat($options);
	        $accessToken = $weObj->checkAuth(Yii::app()->params['weixinAppid'], Yii::app()->params['weixinppsecret']);
	        if($accessToken)
	        	Yii::app()->cache->set('weixin_access_token', $accessToken);
	        else
	        	Yii::log('error', '获取微信access_token失败');
		}

		return $accessToken;
	}

	protected function setFlash($type, $msg)
	{
		Yii::app()->user->setFlash($type, $msg);
	}

	protected function getAuth()
	{
		$file = Yii::getPathOfAlias('application') . '/config/auth.json';
		$content = file_get_contents($file);
		$auths = json_decode($content, true);

		return $auths;
	}
}