<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user = User::model()->login($this->username, $this->password);

		return $this->authenticateByUser($user);
	}

	/**
	 * 微信登录
	 */
	public function authenticateByOpenid($openid)
	{
		$user = User::model()->findByOpenid($openid);

		return $this->authenticateByUser($user);
	}

	private function authenticateByUser($user)
	{
		if(!$user)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id = $user->id;

			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->_id;
	}
}