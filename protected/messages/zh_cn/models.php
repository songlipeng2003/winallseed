<?php

return array(
    'models.AdminGroup'=>'管理员分组',
    'models.Admin'=>'管理员',
    'models.Group'=>'用户分组',
    'models.User'=>'用户',
    'models.Dealer'=>'渠道',
    'models.Doc'=>'文档',
    'models.Display'=>'示范表现',
    'models.Menu'=>'自定义菜单',
    'models.Media'=>'素材',
    'models.Reply'=>'自动回复',
    'models.Article'=>'文章管理',
);