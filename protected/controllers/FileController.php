<?php

class FileController extends Controller
{
    public function actionUpload()
    {
         $result = array('code'=>1);
        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];

            $targetPath = Yii::getPathOfAlias('webroot') . '/upload/tmp/';
            $newFileName = md5(uniqid());
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $newFileName = $newFileName . '.' . $ext;
            $targetFile =  $targetPath . $newFileName;

            @ mkdir($targetPath, 0755, true);
            move_uploaded_file($tempFile, $targetFile);

            $result['url'] = '/upload/tmp/'.$newFileName;
        }

        $this->renderJSON($result);
    }
}