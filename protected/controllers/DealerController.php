<?php

class DealerController extends Controller
{
	public function actionIndex()
	{
		$keyword = Yii::app()->request->getParam('keyword');
		$longitude = Yii::app()->request->getParam('longitude');
		$latitude = Yii::app()->request->getParam('latitude');
		$limit = Yii::app()->request->getParam('limit', 20);
		$limit = $limit>100 ? 100 : $limit;

		$criteria = new CDbCriteria();    
		$criteria->order = 'id desc';        
		$count = Dealer::model()->count($criteria);    
			
		$pager = new CPagination($count);    
		$pager->pageSize = $limit;             
		$pager->applyLimit($criteria);    
	
		$dealers = Dealer::model()->findAll($criteria); 

		echo CJSON::encode($dealers);
	}
}