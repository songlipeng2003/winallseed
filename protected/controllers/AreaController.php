<?php

class AreaController extends Controller
{
    public function actionChildren($id)
    {
        $areas = Area::model()->findAllByAttributes(array('parentId'=>$id));

        $this->renderJSON($areas);
    }
}