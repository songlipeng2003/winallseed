<?php

class DisplayController extends Controller
{
    public function init()
    {
        parent::init();
        if(Yii::app()->user->isGuest){
            Yii::app()->session['return_url'] = '/display/';

            $this->redirect(array('/weixin/user/login'));
        }
    }

    public function actionIndex()
    {
        $this->layout = false;

        $user = User::model()->findByPk(Yii::app()->user->id);
        $auths = $user->group->menu;
        $auths = explode(',', $auths);

        if(array_search('display_index', $auths)===false && array_search('display_create', $auths)===false)
        {
            $this->setFlash('error', '您的权限不够，无法查看，请联系荃银渠道管理人员');
            $this->layout = '/layouts/main';
            $this->render('message');
            exit;
        }

        $this->render('index');
    }

    public function actionList()
    {
        $keyword = Yii::app()->request->getParam('keyword');
        $longitude = Yii::app()->request->getParam('longitude');
        $latitude = Yii::app()->request->getParam('latitude');
        $page = Yii::app()->request->getParam('page', 1);
        $limit = Yii::app()->request->getParam('limit', 20);
        $limit = $limit>100 ? 100 : $limit;

        $criteria = new CDbCriteria();
        $criteria->addCondition("t.status=".Display::STATUS_NORMAL);
        $criteria->addCondition("t.description LIKE '%$keyword%'");;
        $criteria->order = 't.id desc';
        $count = Display::model()->count($criteria);
            
        $pager = new CPagination($count);
        $pager->pageSize = $limit;
        $pager->validateCurrentPage = false;
        $pager->setCurrentPage($page-1);
        $pager->applyLimit($criteria);
    
        $displays = Display::model()->with('user')->findAll($criteria);

        $data = array();
        foreach ($displays as $display) {
            $data[] = array(
                'id'=>$display->id,
                'createTime'=>$display->createTime,
                'description'=>$display->description,
                'img'=>$display->img,
                'latitude'=>$display->latitude,
                'longitude'=>$display->longitude,
                'location'=>$display->location,
                'avatar'=>$display->user ? $display->user->getAvatarUrl(132) : '',
                'nickname'=>$display->user ? $display->user->username : '',
            );
        }

        $result = array(
            'code'=>'0000',
            'info'=>'success',
            'data'=>array(
                'totalPage'=>$pager->getPageCount(),
                'currentPage'=>$pager->getCurrentPage()+1,
                'totalNum'=>$pager->getItemCount(),
                'ary'=>$data
            )
        );

        $this->renderJSON(CJSON::encode($result));
    }

    public function actionUpload()
    {
        $url = '/upload/display/' . strftime('%Y-%m-%d') . '/';
        $dir = Yii::getPathOfAlias('webroot') . $url;
        $fileName = uniqid() . '.jpg';
        @ mkdir($dir, 0777, true);

        $str = file_get_contents("php://input");

        file_put_contents($dir . $fileName, $str);

        $this->renderJSON(array(
            'code'=>'0000',
            'info'=>'success',
            'data'=>$url . $fileName,
        ));
    }

    public function actionCreate()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        if($data['pictureAry']){
            foreach ($data['pictureAry'] as $img) {
                $display = new Display();
                $display->attributes = $data;
                $display->user_id = Yii::app()->user->id;
                $display->img = $img;
                if(!isset($data['location']) || !$data['location']){
                    $display->location = '未知位置';
                }
                $display->save();
            }
        }
        $this->renderJSON(array(
            'code'=>'0000',
            'info'=>'success',
        ));
    }
}