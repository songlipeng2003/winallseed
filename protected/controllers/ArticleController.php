<?php

class ArticleController extends Controller
{

    public function actionView($id)
    {
        $article = Article::model()->findByPk($id);

        $this->layout = false;
        $this->render('view', array(
            'article'=>$article,
        ));
    }

    public function actionReply($id)
    {
        $reply = Reply::model()->findByPk($id);

        $this->layout = false;
        $this->render('view', array(
            'article'=>$reply,
        ));
    }
}