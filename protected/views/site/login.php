<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - 登录';
$this->breadcrumbs=array(
	'登录',
);
?>

<h1>登录</h1>

<p>请输入你的账号密码:</p>

<div class="form">
	<?php 
	$form = $this->beginWidget(
	    'booster.widgets.TbActiveForm',
	    array(
	        'id' => 'inlineForm',
	        'type' => 'horizontal',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
	        'htmlOptions' => array('class' => 'well'),
	    )
	);

		echo $form->textFieldGroup($model, 'username');
		echo $form->passwordFieldGroup($model, 'password');
		echo $form->checkboxGroup($model, 'rememberMe');

		$this->widget(
		    'booster.widgets.TbButton',
		    array('buttonType' => 'submit', 'label' => '登录')
		);

	$this->endWidget();

	unset($form);
	?>
</div><!-- form -->
