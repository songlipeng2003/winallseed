<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>

<h1>Contact Us</h1>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>
If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
</p>

<div class="form">
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php 
	$form = $this->beginWidget(
	    'booster.widgets.TbActiveForm',
	    array(
	        'id' => 'inlineForm',
	        'type' => 'horizontal',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
	        'htmlOptions' => array('class' => 'well'),
	    )
	);

		echo $form->errorSummary($model);

		echo $form->textFieldGroup($model, 'name');
		echo $form->textFieldGroup($model, 'email');
		echo $form->textFieldGroup($model, 'subject');
		echo $form->textAreaGroup($model, 'body');
	?>
	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<div class="col-sm-3 control-label required">
			<?php echo $form->labelEx($model,'verifyCode'); ?>
		</div>
		<div class="col-sm-9">
			<?php $this->widget('CCaptcha'); ?>
			<?php echo $form->textField($model,'verifyCode'); ?>
			<div class="hint">Please enter the letters as they are shown in the image above.
			<br/>Letters are not case-sensitive.</div>
			<?php echo $form->error($model,'verifyCode'); ?>
		</div>
	</div>
	<?php endif; ?>
	<?php

		$this->widget(
		    'booster.widgets.TbButton',
		    array('buttonType' => 'submit', 'label' => 'Submit')
		);

	$this->endWidget();

	unset($form);
	?>
</div><!-- form -->
<?php endif ?>