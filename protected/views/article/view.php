<?php 
$this->pageTitle = $article->title;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
    <?php echo $article->content ?>
</body>
</html>

