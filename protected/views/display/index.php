<?php
Yii::app()->clientScript->reset();
$user = User::model()->findByPk(Yii::app()->user->id);

if($user)
{
    $auths = $user->group->menu;
    $auths = explode(',', $auths);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="target-densitydpi=device-dpi,width=device-width,initial-scale=1">
        <meta name="author" content="Don">
        <link rel="shortcut icon" href="img/logo.jpg">
        <title>示范表现-荃银高科</title>

        <link href="/css/public.css" rel="stylesheet">
        <link href="/css/performance.css" rel="stylesheet">
    </head>
    <body>
        <!--home-->
        <div id="home" class="home">
            <div class="nav">
                <div class="input-group">
                    <input id="searchInput" type="text" class="form-control" placeholder="输入地名或产品名称">
                    <span class="input-group-btn">
                        <div id="searchBtn" class="btn btn-default">
                            <div></div>
                        </div>
                    </span>
                
                    <span class="input-group-btn" <?php if(array_search('display_create', $auths)===false){ ?>
                        style="display:none;"
                    <?php } ?>>
                        <div id="publicBtn" class="btn btn-primary">发布</div>
                    </span>
                </div>
            </div>
            <div class="home-con">
                <div id="home_con0"></div>
                <div id="home_con1"></div>
                <div id="home_con2"></div>
            </div>
        </div>
        <!--desc-->
        <div id="desc" class="">
            <img id="desc_img" />
            <div class="desc-li bg-none">
                <img id="desc_avatar" />
                <div id="desc_userid"></div>
                <div id="desc_time"></div>
            </div>
            <div class="desc-li">
                <img class="loc" src="/images/location.png">
                <div id="desc_location" class="locLab"></div>
            </div>
            <div class="desc-li">
                <div id="desc_desc"></div>
            </div>
            <div id="desc_btn" class="btn btn-default btn-lg">返回</div>
        </div>
        <!--public-->
        <div id="public" class="hide">
            <div class="nav">
                <div class="input-group">
                    <div class="public-navLoc">
                        <img class="loc" src="/images/location.png">
                        <div id="public_loc" class="locLab text-ellipsis">正在获取位置信息...</div>
                    </div>
                    <span class="input-group-btn">
                        <div id="public_send" class="btn btn-success">发送</div>
                    </span>
                </div>
            </div>
            <div class="public-con">
                <textarea id="public_desc" rows="6" type="text" class="form-control" placeholder="说点什么吧..."></textarea>
                <div class="public-conImg">
                    <div id="public_addPic">
                        <img src="/images/plus.png">
                    </div>
                </div>
                <div class="public-conInfo">还可以上传<span id="public_picNum">3</span>张图片</div>
            </div>
            <div id="public_btn" class="btn btn-default btn-lg">返回</div>
            <input id="file" type="file" multiple='multiple'/>
        </div>


        <script src="/js/base.js" type="text/javascript" charset="utf-8"></script>
        <script src="/js/performance.js" type="text/javascript" charset="utf-8"></script>
    </body>
</html>