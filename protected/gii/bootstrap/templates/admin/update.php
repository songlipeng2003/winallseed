<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->modelClass;
echo "\$this->pageTitle = Yii::t('models', 'models.".$label."') . '管理-' . \$this->pageTitle;\n";

echo "\$this->breadcrumbs=array(
	Yii::t('models', 'models.".$label."')=>array('index'),
	\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'更新',
);\n";
?>
?>

<h1>更新<?php echo "<?php echo Yii::t('models', 'models.".$label."') ?>" ?></h1>

<?php echo "<?php echo \$this->renderPartial('_form',array('model'=>\$model)); ?>"; ?>