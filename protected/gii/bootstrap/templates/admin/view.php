<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->modelClass;

echo "\$this->pageTitle = Yii::t('models', 'models.".$label."') . '管理-' . \$this->pageTitle;\n";

echo "\$this->breadcrumbs=array(
	Yii::t('models', 'models.".$label."')=>array('index'),
	\$model->{$nameColumn},
);\n";
?>
?>

<h1>查看<?php echo "<?php echo Yii::t('models', 'models.".$label."') ?>" ?></h1>

<?php echo "<?php"; ?> $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
<?php
foreach ($this->tableSchema->columns as $column) {
	echo "\t\t'" . $column->name . "',\n";
}
?>
),
)); ?>
