<?php
require_once(Yii::app()->basePath.'/vendor/wechat-php-sdk/wechat.class.php');

class CallbackController extends Controller
{
    public function CallbackController($id,$module=null)
    {
        parent::__construct($id,$module=null);

        foreach (Yii::app()->log->routes as $route)
        {
            if($route instanceof CWebLogRoute)
                $route->enabled = false; // disable any weblogroutes

            if($route instanceof YiiDebugToolbarRoute)
                $route->enabled = false; // disable any weblogroutes
        }
    }

    public function actionIndex()
    {
        //$this->weObj->valid(); //注意, 应用验证通过后,可将此句注释掉, 但会降低网站安全性

        $this->weObj->getRev();

        $openid = $this->weObj->getRevFrom();

        $user = User::model()->findByOpenid($openid);
        if(!$user)
        {
            $userInfo = $this->weObj->getUserInfo($openid);

            $user = new User();
            $user->openid = $openid;
            $user->groupId = 1;
            $user->avatar = $userInfo['headimgurl'];
            $user->nickname = $userInfo['nickname'];
            $user->save();
        }

        $type = $this->weObj->getRevType();
        switch($type) 
        {
            case Wechat::MSGTYPE_TEXT:
                $keyword = $this->weObj->getRevContent();
                if(mb_strpos($keyword, '产品搜索+')===0)
                {
                    $product = mb_substr($keyword, 5, NULL, 'utf-8');
                    $this->searchDealersByProduct($product, $user->latitude, $user->longitude);
                    break;
                }
                if(preg_match_all('/#(.*)\+(.*)\+(.*)/', $keyword, $mathes))
                {
                    $product = $mathes[1][0];
                    $city = $mathes[2][0];
                    $area = $mathes[3][0];

                    $this->searchDealersByKeyword($product, $city, $area);
                    break;
                }
                if(!$this->replyByKeyword($keyword))
                    $this->defaultReply();
                break;
            case Wechat::MSGTYPE_EVENT:
                $event = $this->weObj->getRevEvent();

                switch ($event['event']) 
                {
                    case 'subscribe':
                        $welcomeReply = Yii::app()->settings->get('system', 'welcomeReply');
                        $this->replyByKeyword($welcomeReply);
                        break;
                    case 'CLICK':
                        $keyword = $event['key'];
                        if($keyword){
                            $keyword = urldecode($keyword);
                            $this->replyByKeyword($keyword);
                        }
                        break;
                    case 'VIEW':
                        break;
                    case 'LOCATION':
                        $geo = $this->weObj->getRevEventGeo();

                        $latitude = $geo['x'];
                        $longitude = $geo['y'];

                        $user->latitude = $latitude;
                        $user->longitude = $longitude;
                        $user->save();

                        break;
                    default:
                        $this->defaultReply();
                        break;
                }

                break;
            case Wechat::MSGTYPE_LOCATION:
                $this->searchDealersByPoint();
                break;
            case Wechat::MSGTYPE_IMAGE:
                //break;
            case Wechat::MSGTYPE_LINK:
                //break;
            case Wechat::MSGTYPE_MUSIC:
                //break;
            case Wechat::MSGTYPE_NEWS:
                //break;
            case Wechat::MSGTYPE_VOICE:
                //break;
            case Wechat::MSGTYPE_VIDEO:
                //break;
            default:
                $this->defaultReply();
        }
    }

    private function replyByKeyword($keyword)
    {
        $reply = Reply::model()->findByKeyword($keyword);

        if($reply)
        {
            if($reply->type==Reply::TYPE_TEXT)
                $this->weObj->text($reply->content)->reply();
            elseif($reply->type==Reply::TYPE_NEWS){
                $data = array();
                $data[] = array(
                    'Title'=>$reply->title,
                    'Description'=>$reply->description,
                    'PicUrl'=>Yii::app()->getBaseUrl(true) . $reply->cover,
                    'Url'=>Yii::app()->createAbsoluteUrl('article/reply', array('id'=>$reply->id)),
                );
                if($reply->articles){
                    foreach ($reply->articles as $article) {
                        $data[] = 
                        array(
                            'Title'=>$article->title,
                            'Description'=>$article->description,
                            'PicUrl'=>Yii::app()->getBaseUrl(true) . $article->cover,
                            'Url'=>Yii::app()->createAbsoluteUrl('article/view', array('id'=>$article->id)),
                        );
                    }
                }

                $this->weObj->news($data)->reply();
            }
            return true;
        }

        return false;
    }

    private function defaultReply()
    {
        $defaultReply = Yii::app()->settings->get('system', 'defaultReply');
        $this->replyByKeyword($defaultReply);
    }

    private function searchDealersByPoint()
    {
        $geo = $this->weObj->getRevGeo();

        $latitude = $geo['x'];
        $longitude = $geo['y'];

        $dealers = Dealer::model()->findByPoint($latitude, $longitude);

        $this->replyDealers($dealers);
    }

    private function searchDealersByKeyword($product, $city, $area)
    {
        $dealers = Dealer::model()->findByAreaAndKeyword($city, $area, $product);

        $this->replyDealers($dealers);
    }

    private function searchDealersByProduct($product, $latitude, $longitude)
    {
        $dealers = Dealer::model()->findByProduct($product, $latitude, $longitude);

        $this->replyDealers($dealers);
    }

    private function replyDealers($dealers)
    {
        $text = '';
        foreach ($dealers as $key => $dealer) {
            $text .= "名称：" . $dealer->name . "\n";
            $text .= "固话：" . $dealer->phone . "\n";
            $text .= "手机：" . $dealer->mobile . "\n";
            $text .= "地址：" . $dealer->address . "\n";
            $text .= "产品：" . $dealer->product . "\n";
            $text .= "\n";
        }
        if(!$text)
            $text = '没有查找到匹配的经销商';
        $this->weObj->text($text)->reply();
    }
}