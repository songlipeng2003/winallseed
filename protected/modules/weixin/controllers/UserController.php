<?php

class UserController extends Controller
{
    public $layout = '/layouts/main';

    public function init()
    {
        parent::init();

        Yii::app()->booster;
    }

    public function actionLogin()
    {
        $url = $this->weObj->getOauthRedirect(Yii::app()->getBaseUrl(true) . '/weixin/user/callback', 1);
        $this->redirect($url);
    }

    public function actionCallback()
    {
        $result = $this->weObj->getOauthAccessToken();
        if($result)
        {
            $openid = $result['openid'];
            $accessToken = $result['access_token'];

            $session = Yii::app()->session;
            $session['openid'] = $openid;

            $user = User::model()->findByAttributes(array('openid'=>$openid));

            if($user)
            {
                if($user->status==USER::STATUS_APPLY)
                {
                    Yii::app()->user->setFlash('warning', '审核中，等待用户审核中');
                    $this->render("message");
                    Yii::app()->end();
                }elseif($user->status==USER::STATUS_REFUSE){
                    Yii::app()->user->setFlash('warning', '你的资料已拒绝，请重新修改提交');
                    $this->redirect('register');
                }elseif($user->status==User::STATUS_NORMAL){
                    $identity=new UserIdentity(null, null);
                    $identity->authenticateByOpenid($openid);
                    Yii::app()->user->login($identity, 0);

                    if(Yii::app()->session['return_url']){
                        $returnUrl = Yii::app()->session['return_url'];
                        unset(Yii::app()->session['return_url']);
                        $this->redirect($returnUrl);
                    }

                    $this->redirect('/user/');
                }
            }

            $this->redirect(array('register'));
        }else{
            Yii::app()->user->setFlash('warning', '授权失败，请重新授权');
            $this->render("message");
        }
    }

    public function actionRegister()
    {
        $session = Yii::app()->session;
        $openid = isset($session['openid']) ? $session['openid'] : false;
        $accessToken = isset($session['access_token']) ? $session['access_token'] : false;

        if(!$openid)
            throw new CHttpException(404, 'The requested page does not exist.');

        $model = User::model()->findByAttributes(array('openid'=>$openid));
        if(!$model)
        {
            $model = new User;
            $model->openid = $openid;

            $result = $this->weObj->getOauthUserinfo($accessToken, $openid);

            if($result)
            {
                $model->nickname = $result['nickname'];
                $model->avatar = $result['headimgurl'];
                $model->gender = $result['sex'];
                $model->groupId = 0;
            }
        }
        $model->scenario = 'register';
        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            $model->status = User::STATUS_APPLY;
            if($model->save())
            {
                $this->setFlash('success', '注册信息已经提交，正在审核');
                $this->render('message');
                Yii::app()->end();
            }
        }

        $this->render('register', array(
            'model'=>$model
        ));
    }
}