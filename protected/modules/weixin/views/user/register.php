<h1>注册</h1>

<div class="form">
    <?php 
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'inlineForm',
            'type' => 'horizontal',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
            'htmlOptions' => array('class' => 'well'),
        )
    );

        echo $form->textFieldGroup($model,'mobile',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255))));

        echo $form->textFieldGroup($model, 'username');

        echo $form->dropDownListGroup($model,'groupId',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array(
                    'class'=>'span5',
                    'maxlength'=>255
                ),
                'data'=>CHtml::listData(Group::model()->findAll(), 'id', 'name')
            )
        ));

        echo $form->dropDownListGroup($model,'gender',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array(
                    'class'=>'span5',
                    'maxlength'=>255
                ),
                'data'=>Constants::$genders
            )
        ));

        echo $form->textFieldGroup($model,'phone',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255))));

        echo $form->dropDownListGroup($model,'provinceId',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array(
                    'class'=>'span5',
                    'maxlength'=>255,
                    'empty'=>'请选择'
                ),
                'data'=>CHtml::listData(Area::model()->findAllTop(), 'id', 'name')
            ),
        ));

        echo $form->dropDownListGroup($model,'cityId',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array(
                    'class'=>'span5',
                    'maxlength'=>255,
                    'empty'=>'请选择'
                ),
                'data'=>CHtml::listData(Area::model()->findAllByParent($model->provinceId), 'id', 'name'),
            ),
        ));

        echo $form->dropDownListGroup($model,'areaId',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array(
                    'class'=>'span5',
                    'maxlength'=>255,
                    'empty'=>'请选择'
                ),
                'data'=>CHtml::listData(Area::model()->findAllByParent($model->cityId), 'id', 'name')
            ),
        ));

        echo $form->textFieldGroup($model,'address',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255))));

        $this->widget(
            'booster.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => '注册', 'block'=>true, 'loadingText'=>'加载中', 'context'=>'primary')
        );

    $this->endWidget();

    unset($form);
    ?>
</div><!-- form -->

<script type="text/javascript">
$(function(){
    area_select($('#User_provinceId'), $('#User_cityId'), $('#User_areaId'));
});
</script>
