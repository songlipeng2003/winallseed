<ul class="list-group">
<?php if(in_array('display_index', $menus)){ ?>
    <li class="list-group-item"><?php echo CHtml::link('示范表现-查询', '/display', array('class'=>'btn btn-default btn-block')) ?></li>
<?php } ?>
<?php if(in_array('display_create', $menus)){ ?>
    <li class="list-group-item"><?php echo CHtml::link('示范表现-发布', '/display#public', array('class'=>'btn btn-default btn-block')) ?></li>
<?php } ?>
<?php if(in_array('prevent_query', $menus)){ ?>
    <li class="list-group-item"><?php echo CHtml::link('防窜查询', array('#'), array('class'=>'btn btn-default btn-block'), array('onclick'=>'return false;')) ?></li>
<?php } ?>
</ul>