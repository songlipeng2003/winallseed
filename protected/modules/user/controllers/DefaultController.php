<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
        $user = User::model()->findByPk(Yii::app()->user->id);
        $menus = $user->group->menu ? explode(',', $user->group->menu) : array();

		$this->render('index', array(
            'user'=>$user,
            'menus'=>$menus
        ));
	}
}