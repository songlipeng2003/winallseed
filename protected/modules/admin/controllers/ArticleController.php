<?php

class ArticleController extends AdminController
{
    /**
    * @return array action filters
    */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
    * Specifies the access control rules.
    * This method is used by the 'accessControl' filter.
    * @return array access control rules
    */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('*'),
                'users'=>array('admin'),
            ),
        );
    }

    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
    public function actionCreate($id)
    {
        $model=new Article('create');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Article']))
        {
            $model->attributes=$_POST['Article'];
            $model->replyId = $id;
            if($model->save())
            {
                $this->redirect(array('reply/view','id'=>$id));
            }
        }

        $reply = Reply::model()->findByPk($id);

        if(sizeof($reply->articles)>9){
            $this->setFlash('error', '一个回复最多只能10篇文章');
            $this->redirect(array('reply/view','id'=>$id));
        }

        $this->render('create',array(
            'model'=>$model,
            'reply'=>$reply
        ));
    }

    /**
    * Updates a particular model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id the ID of the model to be updated
    */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Article']))
        {
            $model->attributes=$_POST['Article'];
            if($model->validate())
            {
                if($model->cover)
                {
                    $model->save();
                }
                $this->redirect(array('reply/view','id'=>$model->replyId));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
    * Deletes a particular model.
    * If deletion is successful, the browser will be redirected to the 'admin' page.
    * @param integer $id the ID of the model to be deleted
    */
    public function actionDelete($id)
    {
        // if(Yii::app()->request->isPostRequest)
        // {
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('reply/view', 'id'=>$model->replyId));
        // }
        // else
        //     throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    /**
    * Manages all models.
    */
    public function actionIndex($id)
    {
        $model=new Article('search');
        $model->unsetAttributes();  // clear any default values
        $model->replyId = $id;
        if(isset($_GET['Article']))
        {
            $model->attributes=$_GET['Article'];
        }

        $reply = Reply::model()->findByPk($id);
        $this->render('index',array(
            'model'=>$model,
            'reply'=>$reply,
        ));
    }

    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    * @param integer the ID of the model to be loaded
    */
    public function loadModel($id)
    {
        $model=Article::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='article-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
