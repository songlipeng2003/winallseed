<?php

class MenuController extends AdminController
{
    public function actionIndex()
    {
        $model=new Menu('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Menu']))
            $model->attributes=$_GET['Menu'];

        $menu = Yii::app()->settings->get('system', 'weixinMenu');

        $this->render('index',array(
            'model'=>$model,
            'menu'=>$menu,
            'weixinMenuLastModify'=>Yii::app()->settings->get('system', 'weixinMenuLastModify')
        ));
    }

    public function actionSave()
    {

        if(Yii::app()->request->isPostRequest)
        {
            $postdata = file_get_contents("php://input");
            $post = json_decode($postdata, true);

            $menu = $post['menus'];

            Yii::app()->settings->set('system', 'weixinMenu', json_encode($menu));
            Yii::app()->settings->set('system', 'weixinMenuLastModify', time());

            $menu = $this->menuBuildMenuSet($menu);
            if($this->weObj->createMenu($menu)){
                $this->renderJSON('success');
            }

            $this->renderJSON(array('message'=>$this->weObj->errMsg));            
        }
    }

    public function actionDelete()
    {
        if(Yii::app()->request->isPostRequest)
        { 
            if($this->weObj->deleteMenu()){
                Yii::app()->settings->set('system', 'weixinMenu', array());

                $this->renderJSON('success');
            }

            $this->renderJSON(array('message'=>$this->weObj->errMsg));
        }
    }

    private function menuBuildMenuSet($menu) {
        $types = array(
            'view', 'click', 'scancode_push', 
            'scancode_waitmsg', 'pic_sysphoto', 'pic_photo_or_album', 
            'pic_weixin', 'location_select'
        );
        $set = array();
        $set['button'] = array();
        foreach($menu as $m) {
            $entry = array();
            $entry['name'] = $m['title'];
            if(!empty($m['subMenus'])) {
                $entry['sub_button'] = array();
                foreach($m['subMenus'] as $s) {
                    $e = array();
                    if ($s['type'] == 'url') {
                        $e['type'] = 'view';
                    } elseif (in_array($s['type'], $types)) {
                        $e['type'] = $s['type'];
                    } else {
                        $e['type'] = 'click';
                    }
                    $e['name'] = $s['title'];
                    if($e['type'] == 'view') {
                        $e['url'] = $s['url'];
                    } else {
                        $e['key'] = urlencode($s['forward']);
                    }
                    $entry['sub_button'][] = $e;
                }
            } else {
                if ($m['type'] == 'url') {
                    $entry['type'] = 'view';
                } elseif (in_array($m['type'], $types)) {
                    $entry['type'] = $m['type'];
                } else {
                    $entry['type'] = 'click';
                }
                if($entry['type'] == 'view') {
                    $entry['url'] = $m['url'];
                } else {
                    $entry['key'] = urlencode($m['forward']);
                }
            }
            $set['button'][] = $entry;
        }
        return $set;
    }
}
