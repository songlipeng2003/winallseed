<?php
require_once(Yii::app()->basePath.'/vendor/wechat-php-sdk/wechat.class.php');

class GroupController extends AdminController
{

    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
    public function actionCreate()
    {
        $model=new Group;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            $model->attributes=$_POST['Group'];

            if($model->save())
                $this->redirect(array('index'));
            else
                $this->setFlash('error', '创建失败');
        }

        $auths = $this->getAuth();

        $this->render('create', array(
            'model'=>$model,
            'auths'=>$auths['user']
        ));
    }

    /**
    * Updates a particular model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id the ID of the model to be updated
    */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            $model->attributes=$_POST['Group'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $auths = $this->getAuth();
        $model->menu = explode(',', $model->menu);

        $this->render('update',array(
            'model'=>$model,
            'auths'=>$auths['user']
        ));
    }

    /**
    * Deletes a particular model.
    * If deletion is successful, the browser will be redirected to the 'admin' page.
    * @param integer $id the ID of the model to be deleted
    */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            User::model()->updateAll(array('groupId'=>0), "groupId=$id");

            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    /**
    * Manages all models.
    */
    public function actionIndex()
    {
        $model=new Group('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Group']))
            $model->attributes=$_GET['Group'];

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    public function actionSync()
    {
        $options = array(
            'token'=>Yii::app()->params['weixinToken'], //填写你设定的key
            'appid'=>Yii::app()->params['weixinAppid'], //填写高级调用功能的app id, 请在微信开发模式后台查询
            'appsecret'=>Yii::app()->params['weixinppsecret'], //填写高级调用功能的密钥
        );

        $weObj = new Wechat($options);

        $result = $weObj->getGroup();
        $groups = $result['groups'];

        foreach ($groups as $group) 
        {
            $userGroup = Group::model()->findByName($group['name']);
            if(!$userGroup)
            {
                $userGroup = new Group();
                $userGroup->id = $group['id'];
                $userGroup->name = $group['name'];
                // $userGroup->count = $group['count'];
                $userGroup->save();
            }
        }
    }

    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    * @param integer the ID of the model to be loaded
    */
    public function loadModel($id)
    {
        $model=Group::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='group-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
