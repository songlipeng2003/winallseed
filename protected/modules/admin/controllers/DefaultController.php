<?php

class DefaultController extends AdminController
{
	public function actionIndex()
	{
        $displayApplyNumber = Display::model()->countByAttributes(array('status'=>Display::STATUS_APPLY));
        $userApplyNumber = User::model()->countByAttributes(array('status'=>User::STATUS_APPLY));

		$this->render('index', array(
            'displayApplyNumber'=>$displayApplyNumber,
            'userApplyNumber'=>$userApplyNumber,
        ));
	}
}