<?php

class AdminController extends Controller
{
    public $layout = '/layouts/column1';

    public function init()
    {
        parent::init();

        $adminUser = Yii::app()->adminUser;

        if($adminUser->isGuest)
            $this->redirect(array('site/login'));
    }

    /**
    * @return array action filters
    */
    // public function filters()
    // {
    //     return array(
    //         'accessControl', // perform access control for CRUD operations
    //     );
    // }

    /**
    * Specifies the access control rules.
    * This method is used by the 'accessControl' filter.
    * @return array access control rules
    */
    // public function accessRules()
    // {
    //     return array(
    //         array('allow',
    //             'actions'=>array('index','view','create','update','delete'),
    //             'users'=>array('admin'),
    //         ),
    //         array('deny',
    //             'users'=>array('*'),
    //         ),
    //     );
    // }
}