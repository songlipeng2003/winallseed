<?php

/**
 * 管理员登录逻辑
 */
class AdminIdentity extends CUserIdentity
{
    private $_id;

    /**
     * 判断登录是否成功
     *
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $admin = Admin::model()->login($this->username, $this->password);

        if(!$admin || $admin->status==Admin::STATUS_HIDDEN)
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->_id = $admin->id;
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}