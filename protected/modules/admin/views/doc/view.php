<?php
$this->pageTitle = Yii::t('models', 'models.Doc') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
	Yii::t('models', 'models.Doc')=>array('index'),
	$model->title,
);
?>

<h1>查看<?php echo Yii::t('models', 'models.Doc') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
		array(
            'name'=>'content',
            'type'=>'raw'
        ),
		'createTime',
		'updateTime',
),
)); ?>
