<?php
$this->pageTitle = Yii::t('models', 'models.Doc') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
    Yii::t('models', 'models.Doc')=>array('index'),
    '管理',
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('doc-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<h1><?php echo Yii::t('models', 'models.Doc') ?>管理</h1>

<?php echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>创建'.Yii::t('models', 'models.Doc'), array('create'), array('class'=>'btn btn-primary')) ?><br/>
<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'doc-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
		'title',
		'createTime',
		'updateTime',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
        ),
    ),
)); ?>
