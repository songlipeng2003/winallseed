<?php
$this->pageTitle = Yii::t('models', 'models.Article') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
    '自动回复'=>array('reply/index'),
    Yii::t('models', 'models.Article')=>array('index', 'id'=>$reply->id),
    '管理',
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('article-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<h1><?php echo Yii::t('models', 'models.Article') ?>管理</h1>

<?php echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>创建'.Yii::t('models', 'models.Article'), array('create', 'id'=>$reply->id), array('class'=>'btn btn-primary')) ?><br/>
<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'article-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
		'title',
		array(
            'name'=>'cover',
            'type'=>'raw',
            'value'=>"CHtml::image(\$data->cover, '', array('width'=>300))",
            'filter'=>false,
        ),
		'createTime',
		'updateTime',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
        ),
    ),
)); ?>
