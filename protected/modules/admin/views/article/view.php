<?php
$this->pageTitle = Yii::t('models', 'models.Article') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
    $model->reply->name=>array('reply/view', 'id'=>$model->replyId),
	$model->title,
);
?>

<h1>查看<?php echo Yii::t('models', 'models.Article') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
        array(
            'name'=>'cover',
            'type'=>'raw',
            'value'=>CHtml::image($model->cover)
        ),
		'description',
		array(
            'name'=>'content',
            'type'=>'raw'
        ),
		'createTime',
		'updateTime',
),
)); ?>
