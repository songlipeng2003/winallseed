<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'article-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true
	),
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'
    )
)); ?>

<p class="help-block">带 <span class="required">*</span> 必填.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->hiddenField($model,'cover'); ?>

    <div class="form-group">
        <label class="col-sm-3 control-label required" for="Reply_description">封面<span class="required">*</span></label>
        <div class="col-sm-9">
            <div id="uploader-demo">
                <!--用来存放item-->
                <div id="fileList" class="uploader-list">
                    <?php if($model->cover){echo CHtml::image($model->cover, '', array('width'=>'320'));} ?>
                </div>
                <div id="filePicker">选择图片</div>
                <div class="hint">封面（大图片建议尺寸：200像素 * 200像素）</div>
            </div>
        </div>
    </div>

	<?php echo $form->textFieldGroup($model,'description',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<div class="form-group">
        <div class="col-sm-3 control-label required">内容<span class="required">*</span></div>
        <div class="col-sm-9">
            <script id="editor" name="Article[content]" type="text/plain"><?php echo $model->content ?></script>
        </div>
    </div>

    <?php
    $this->widget('ext.ueditor.UeditorWidget',
            array(
                'id'=>'editor',//页面中输入框（或其他初始化容器）的ID
                'name'=>'editor',//指定ueditor实例的名称,个页面有多个ueditor实例时使用
            )
        );
    ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? '创建' : '更新',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<link rel="stylesheet" type="text/css" href="/css/webuploader.css">
<script type="text/javascript" src="/js/webuploader/webuploader.js"></script>
<script type="text/javascript">
var uploader = WebUploader.create({
    // 选完文件后，是否自动上传。
    auto: true,

    // swf文件路径
    swf: '/swf/Uploader.swf',

    // 文件接收服务端。
    server: '/file/upload',

    // 选择文件的按钮。可选。
    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
    pick: '#filePicker',

    // 只允许选择图片文件。
    accept: {
        title: 'Images',
        extensions: 'gif,jpg,jpeg,bmp,png',
        mimeTypes: 'image/*'
    }
});

uploader.on( 'fileQueued', function( file ) {
    // $list为容器jQuery实例
    $img = $('.uploader-list').html('<img />').find('img');

    // 创建缩略图
    // 如果为非图片文件，可以不用调用此方法。
    // thumbnailWidth x thumbnailHeight 为 100 x 100
    uploader.makeThumb( file, function( error, src ) {
        if ( error ) {
            $img.replaceWith('<span>不能预览</span>');
            return;
        }

        $img.attr( 'src', src );
    }, 320, 280 );
});

uploader.on( 'uploadAccept', function(object, ret) {
    $('#Article_cover').val(ret.url);
});
</script>