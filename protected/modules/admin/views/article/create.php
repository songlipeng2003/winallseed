<?php
$this->pageTitle = Yii::t('models', 'models.Article') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
	$reply->name=>array('reply/view', 'id'=>$reply->id),
	'创建',
);
?>

<h1>创建<?php echo Yii::t('models', 'models.Article') ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>