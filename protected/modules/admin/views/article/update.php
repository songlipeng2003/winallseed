<?php
$this->pageTitle = Yii::t('models', 'models.Article') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
	$model->reply->name=>array('reply/view','id'=>$model->replyId),
	'更新',
);
?>

<h1>更新<?php echo Yii::t('models', 'models.Article') ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>