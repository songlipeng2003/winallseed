<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="language" content="en"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="/js/common.js"></script>
</head>
<body>
    <?php
    $admin = Admin::model()->findByPk(Yii::app()->adminUser->id);

    if($admin)
    {
        $auths = $admin->adminGroup->menu;
        $auths = explode(',', $auths);

        $navs = array();

        $navs[] = array('label' => '首页', 'url' => array('/admin/default/index'));

        if(array_search('adminGroup', $auths)!==false || array_search('admin', $auths)!==false){
            $menu = array(
                'class' => 'booster.widgets.TbMenu',
                'type' => 'navbar',
                'url'=>'#',
                'label'=>'管理员',
                'active' => in_array(Yii::app()->controller->id, array('adminGroup', 'admins')),
                'items' => array()
            );

            $items = array();
            if(array_search('adminGroup', $auths)!==false){
                $items[] = array('label' => '管理员分组', 'url' => array('adminGroup/'));
            }
            if(array_search('admins', $auths)!==false){
                $items[] = array('label' => '管理员', 'url' => array('admins/'));
            }
            $menu['items'] = $items;
            $navs[] = $menu;
        }

        if(array_search('group', $auths)!==false || array_search('user', $auths)!==false){
            $menu = array(
                'class' => 'booster.widgets.TbMenu',
                'type' => 'navbar',
                'url'=>'#',
                'label'=>'用户',
                'active' => in_array(Yii::app()->controller->id, array('group', 'user')),
                'items' => array()
            );

            $items = array();
            if(array_search('group', $auths)!==false){
                $items[] = array('label' => '用户分组', 'url' => array('group/'));
            }
            if(array_search('user', $auths)!==false){
                $items[] = array('label' => '用户', 'url' => array('user/'));
            }
            $menu['items'] = $items;
            $navs[] = $menu;
        }

        if(array_search('dealer', $auths)!==false){
            $navs[] = array('label' => '渠道', 'url' => array('/admin/dealer/index'));
        }

        if(array_search('display', $auths)!==false){
            $navs[] = array('label' => '示范展示', 'url' => array('/admin/display/index'));
        }

        if(array_search('reply', $auths)!==false){
            $navs[] = array(
                'class' => 'booster.widgets.TbMenu',
                'type' => 'navbar',
                'url'=>'#',
                'label'=>'自动回复',
                'active' => in_array(Yii::app()->controller->id, array('reply')),
                'items' => array(
                    array('label' => '自动回复', 'url' => array('reply/')),
                    array('label' => '回复设置', 'url' => array('reply/setting')),
                )
            );
        }

        if(array_search('menu', $auths)!==false){
            $navs[] = array('label' => '自定义菜单', 'url' => array('/admin/menu/'));
        }

        $navs = array(array(
            'class' => 'booster.widgets.TbMenu',
            'type' => 'navbar',
            'items' => $navs,
        ));

        $navs[] = array(
            'class' => 'booster.widgets.TbMenu',
            'type' => 'navbar',
            'htmlOptions' => array('class' => 'pull-right'),
            'items' => array(
                array('label'=>'注销 ('.Yii::app()->adminUser->name.')', 'url'=>array('/admin/site/logout'), 'visible'=>!Yii::app()->adminUser->isGuest)
            )
        );
    }else{
       $navs = array(); 
    }
    ?>

    <?php
        $this->widget(
            'booster.widgets.TbNavbar',
            array(
                'brand' => CHtml::encode(Yii::app()->name),
                'fixed' => false,
                'fluid' => false,
                'items' => $navs,
            )
        );
    ?>
    
    <?php if(isset($this->breadcrumbs)):?>
    <div class="container">
        <?php $this->widget('booster.widgets.TbBreadcrumbs', array(
            'homeLink'=>CHtml::link('首页', array('/admin/')),
            'links'=>$this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
    </div>
    <?php endif?>

    <div class="container">
        <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {
                $key = $key=='error'?'danger':$key;
                echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
            }
        ?>
    </div>

    <?php echo $content; ?>

    <footer class="footer">
        <div class="container">
            <p class="powered">
                Copyright &copy; 2014 by <?php echo CHtml::encode(Yii::app()->name); ?> </p>
        </div>
    </footer>
</body>
</html>
