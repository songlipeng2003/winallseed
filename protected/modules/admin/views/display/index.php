<?php
$this->pageTitle = Yii::t('models', 'models.Display') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
    Yii::t('models', 'models.Display')=>array('index'),
    '管理',
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('display-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<h1><?php echo Yii::t('models', 'models.Display') ?>管理</h1>

<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'display-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        array(
            'name'=>'user_id',
            'value'=>'$data->user ? $data->user->username : ""',
            'htmlOptions'=>array('width'=>120),
        ),
        array(
            'name'=>'img',
            'type'=>'raw',
            'value'=>'CHtml::image($data->img, "", array("width"=>300))'
        ),
        array(
            'name'=>'status',
            'filter'=>CHtml::activeDropDownList($model, 'status', Display::$statuses, array(
                'class'=>'form-control',
                'empty'=>'请选择'
            )),
            'value'=>'Display::$statuses[$data->status]',
            'htmlOptions'=>array('width'=>100),
        ),
		'createTime',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{view} {update} {delete} {confirm} {refuse}',
            'buttons'=>array(
                'confirm'=>array(
                    'label'=>'同意',
                    'url'=>"Yii::app()->createUrl('admin/display/confirm', array('id'=>\$data->id))",
                    'visible'=>'$data->status==Display::STATUS_APPLY',
                    'click'=>'function(){return confirm("你确认同意吗？")}',
                ),
                'refuse'=>array(
                    'label'=>'拒绝',
                    'url'=>"Yii::app()->createUrl('admin/display/refuse', array('id'=>\$data->id))",
                    'visible'=>'$data->status==Display::STATUS_APPLY',
                    'click'=>'function(){return confirm("你确认拒绝吗？")}',
                ),
            )
        ),
    ),
));
