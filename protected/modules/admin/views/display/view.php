<?php
$this->pageTitle = Yii::t('models', 'models.Display') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
	Yii::t('models', 'models.Display')=>array('index'),
	$model->id,
);
?>

<h1>查看<?php echo Yii::t('models', 'models.Display') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		array(
            'name'=>'user_id',
            'value'=>$model->user ? $model->user->username : "",
        ),
		array(
            'name'=>'img',
            'type'=>'raw',
            'value'=>CHtml::image($model->img, "", array("width"=>300))
        ),
		'description',
		'longitude',
		'latitude',
		'location',
		'createTime',
		'updateTime',
),
)); ?>
