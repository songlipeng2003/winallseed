<?php
$this->pageTitle = Yii::t('models', 'models.Media') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
	Yii::t('models', 'models.Media')=>array('index'),
	$model->id,
);
?>

<h1>查看<?php echo Yii::t('models', 'models.Media') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'type',
		'media',
		'filename',
		'filelength',
		'url',
		'mediaId',
		'createTime',
		'updateTime',
),
)); ?>
