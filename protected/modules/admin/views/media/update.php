<?php
$this->pageTitle = Yii::t('models', 'models.Media') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
	Yii::t('models', 'models.Media')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'更新',
);
?>

<h1>更新<?php echo Yii::t('models', 'models.Media') ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>