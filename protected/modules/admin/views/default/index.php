<?php if($userApplyNumber){ ?>
<div class="alert alert-info" role="alert">
    有<?php echo $userApplyNumber; ?>待审核用户,
    <?php echo CHtml::link('点击这里审核', array('user/index'), array('class'=>'alert-link')); ?>
</div>
<?php } ?>

<?php if($displayApplyNumber){ ?>
<div class="alert alert-info" role="alert">
    有<?php echo $displayApplyNumber; ?>待审核的示范展示,
    <?php echo CHtml::link('点击这里审核', array('display/index'), array('class'=>'alert-link')); ?>
</div>
<?php } ?>