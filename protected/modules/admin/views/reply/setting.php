<?php
$this->pageTitle = '自动回复设置-' . $this->pageTitle;
$this->breadcrumbs=array(
    Yii::t('models', 'models.Reply')=>array('index'),
    '设置',
);
?>

<h1><?php echo Yii::t('models', 'models.Reply') ?>设置</h1>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
    'id'=>'school-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true
    ),
)); ?>

<p class="help-block">带 <span class="required">*</span> 必填.</p>

<?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'defaultReply',array(
        'widgetOptions'=>array(
            'htmlOptions'=>array(
                'class'=>'span5',
                'maxlength'=>255,
                'placeholder'=>'可根据关键字直接关联指定的回复规则'
            )
        ),
        'hint'=>'当系统不知道该如何回复粉丝的消息时，默认发送的内容。
指定系统不知道该如何回复粉丝的消息时，发送的默认信息, 你可以在这里输入关键字, 那么系统不知道该如何回复粉丝的消息时就相当于发送这个内容至微擎系统
这个过程是程序模拟的, 比如这里添加关键字: ￥@%&%#@*, 系统不知道该如何回复粉丝的消息, 微擎系统相当于接受了粉丝用户的消息, 内容为"欢迎关注"'
    )); ?>

    <?php echo $form->textFieldGroup($model,'welcomeReply',array(
        'widgetOptions'=>array(
            'htmlOptions'=>array(
                'class'=>'span5',
                'maxlength'=>255,
                'placeholder'=>'可根据关键字直接关联指定的回复规则'
            )
        ),
        'hint'=>'设置用户添加公众帐号好友时，发送的欢迎信息。 表情
指定用户添加公众帐号好友时，发送的欢迎信息, 你可以在这里输入关键字, 那么用户添加公众号好友时就相当于发送这个内容至微擎系统
这个过程是程序模拟的, 比如这里添加关键字: 欢迎关注, 那么用户添加公众号好友时, 微擎系统相当于接受了粉丝用户的消息, 内容为"欢迎关注"'
    )); ?>

<div class="form-actions">
    <?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'submit',
            'context'=>'primary',
            'label'=>'保存',
        )); ?>
</div>

<?php $this->endWidget(); ?>
