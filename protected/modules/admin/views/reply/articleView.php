<?php
$this->pageTitle = Yii::t('models', 'models.Reply') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
    Yii::t('models', 'models.Reply')=>array('index'),
    $model->name,
);
?>

<h1>查看<?php echo Yii::t('models', 'models.Reply') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
        'id',
        'name',
        'keywords',
        'title',
        'description',
        array(
            'name'=>'cover',
            'type'=>'raw',
            'value'=>CHtml::image($model->cover)
        ),
        array(
            'name'=>'content',
            'type'=>'raw'
        ),

        'createTime',
        'updateTime',
),
)); ?>

<?php echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>创建文章', array('article/create', 'id'=>$model->id), array('class'=>'btn btn-primary')) ?><br/>


<table class="table">
    <thead>
        <tr>
            <td>标题</td>
            <td>封面</td>
            <td>描述</td>
            <td>创建时间</td>
            <td>操作</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($model->articles as $article) { ?>
        <tr>
            <td><?php echo $article->title ?></td>
            <td><?php echo CHtml::image($article->cover, '', array('width'=>300)) ?></td>
            <td><?php echo $article->description ?></td>
            <td><?php echo $article->createTime ?></td>
            <td>
                <?php 
                echo CHtml::link('<i class="glyphicon glyphicon-eye-open"></i>', array('article/view', 'id'=>$article->id), array('title'=>'查看'));
                echo CHtml::link('<i class="glyphicon glyphicon-pencil"></i>', array('article/update', 'id'=>$article->id), array('title'=>'更新'));
                echo CHtml::link('<i class="glyphicon glyphicon-trash"></i>', array('article/delete', 'id'=>$article->id), array('title'=>'删除'));
                ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
