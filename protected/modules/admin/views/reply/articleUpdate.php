<?php
$this->pageTitle = Yii::t('models', 'models.Reply') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
    Yii::t('models', 'models.Reply')=>array('index'),
    $model->name=>array('view','id'=>$model->id),
    '更新',
);
?>

<h1>更新图文<?php echo Yii::t('models', 'models.Reply') ?></h1>

<?php echo $this->renderPartial('_articleForm', array('model'=>$model)); ?>