<?php
$this->pageTitle = Yii::t('models', 'models.Reply') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
    Yii::t('models', 'models.Reply')=>array('index'),
    '管理',
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('reply-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<h1><?php echo Yii::t('models', 'models.Reply') ?>管理</h1>

<?php echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>创建文字'.Yii::t('models', 'models.Reply'), array('create'), array('class'=>'btn btn-primary')) ?>&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>创建图文'.Yii::t('models', 'models.Reply'), array('articleCreate'), array('class'=>'btn btn-primary')) ?><br/>
<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'reply-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
		'name',
        'keywords',
        array(
            'name'=>'type',
            'value'=>'Reply::$types[$data->type]',
            'filter'=>CHtml::activeDropDownList($model, 'type', Reply::$types, array(
                'class'=>'form-control',
                'empty'=>'请选择'
            )),
            'htmlOptions'=>array('width'=>100),
        ),
		'createTime',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            // 'template'=>'{view} {update} {delete}',
            // 'buttons'=>array(
            //     'confirm'=>array(
            //         'label'=>'文章',
            //         'url'=>"Yii::app()->createUrl('admin/user/confirm', array('id'=>\$data->id))",
            //         'visible'=>'$data->status==User::STATUS_APPLY',
            //         'click'=>'function(){return confirm("你确认同意吗？")}',
            //     ),
            // )
        ),
    ),
)); ?>
