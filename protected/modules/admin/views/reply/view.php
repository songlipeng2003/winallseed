<?php
$this->pageTitle = Yii::t('models', 'models.Reply') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
    Yii::t('models', 'models.Reply')=>array('index'),
    $model->name,
);
?>

<h1>查看<?php echo Yii::t('models', 'models.Reply') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
        'id',
        'name',
        'keywords',
        'content',
        'createTime',
        'updateTime',
),
)); ?>