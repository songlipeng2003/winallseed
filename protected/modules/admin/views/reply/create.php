<?php
$this->pageTitle = Yii::t('models', 'models.Reply') . '管理-' . $this->pageTitle;
$this->breadcrumbs=array(
	Yii::t('models', 'models.Reply')=>array('index'),
	'创建',
);
?>

<h1>创建<?php echo Yii::t('models', 'models.Reply') ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>