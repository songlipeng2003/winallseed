<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true
	),
)); ?>

<p class="help-block">带 <span class="required">*</span> 必填.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'groupId',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255
			),
			'data'=>CHtml::listData(Group::model()->findAll(), 'id', 'name')
		),
	)); ?>

	<?php echo $form->textFieldGroup($model,'note',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255
			)
		),
	)); ?>

	<?php echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'gender',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255
			),
			'data'=>Constants::$genders
		)
	)); ?>

	<?php echo $form->textFieldGroup($model,'mobile',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'phone',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'provinceId',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255,
                'empty'=>'请选择'
			),
			'data'=>CHtml::listData(Area::model()->findAllTop(), 'id', 'name')
		),
	)); ?>

	<?php echo $form->dropDownListGroup($model,'cityId',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255,
                'empty'=>'请选择'
			),
			'data'=>CHtml::listData(Area::model()->findAllByParent($model->provinceId), 'id', 'name'),
		),
	)); ?>

	<?php echo $form->dropDownListGroup($model,'areaId',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255,
                'empty'=>'请选择'
			),
			'data'=>CHtml::listData(Area::model()->findAllByParent($model->cityId), 'id', 'name')
		),
	)); ?>

	<?php echo $form->textFieldGroup($model,'address',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'status',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255,
                'empty'=>'请选择'
			),
			'data'=>User::$statuses
		)
	)); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? '创建' : '更新',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
$(function(){
	area_select($('#User_provinceId'), $('#User_cityId'), $('#User_areaId'));
});
</script>
