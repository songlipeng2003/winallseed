<?php
$this->pageTitle = Yii::t('models', 'models.User') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
	Yii::t('models', 'models.User')=>array('index'),
	$model->id,
);
?>

<h1>查看<?php echo Yii::t('models', 'models.User') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'openid',
        array(
            'name'=>'groupId',
            'value'=>$model->groupId ? $model->group->name : '未分组'
        ),
        array(
            'name'=>'avatar',
            'type'=>'raw',
            'value'=>$model->avatar ? CHtml::image($model->getAvatarUrl(132)) : ""
        ),
		'username',
		'nickname',
        array(
            'name'=>'gender',
            'value'=>Constants::$genders[$model->gender]
        ),
		'mobile',
		'phone',
		'address',
        array(
            'name'=>'status',
            'value'=>User::$statuses[$model->status]
        ),
		'note',
		'createTime',
		'updateTime',
),
)); ?>
