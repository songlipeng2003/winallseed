<?php
$this->pageTitle = Yii::t('models', 'models.User') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
    Yii::t('models', 'models.User')=>array('index'),
    '管理',
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('user-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<style type="text/css">
#user-grid_c0{width: 50px;}
</style>

<h1><?php echo Yii::t('models', 'models.User') ?>管理</h1>

<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'user-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        array(
            'name'=>'groupId',
            'value'=>'$data->groupId ? $data->group->name : "未分组"',
            'filter'=>CHtml::activeDropDownList($model, 'groupId', CHtml::listData(Group::model()->findAll(), 'id', 'name'), array(
                'class'=>'form-control',
                'empty'=>'请选择'
            )),
            'htmlOptions'=>array('width'=>120),
        ),
        'openid',
        'note',
        array(
            'name'=>'avatar',
            'type'=>'raw',
            'value'=>'$data->avatar ? CHtml::image($data->getAvatarUrl(64)) : ""'
        ),
		'nickname',
        'mobile',
        array(
            'name'=>'status',
            'filter'=>CHtml::activeDropDownList($model, 'status', User::$statuses, array(
                'class'=>'form-control',
                'empty'=>'请选择'
            )),
            'value'=>'User::$statuses[$data->status]',
            'htmlOptions'=>array('width'=>100),
        ),
		'createTime',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{view} {update} {delete} {confirm} {refuse}',
            'buttons'=>array(
                'confirm'=>array(
                    'label'=>'同意',
                    'url'=>"Yii::app()->createUrl('admin/user/confirm', array('id'=>\$data->id))",
                    'visible'=>'$data->status==User::STATUS_APPLY',
                    'click'=>'function(){return confirm("你确认同意吗？")}',
                ),
                'refuse'=>array(
                    'label'=>'拒绝',
                    'url'=>"Yii::app()->createUrl('admin/user/refuse', array('id'=>\$data->id))",
                    'visible'=>'$data->status==User::STATUS_APPLY',
                    'click'=>'function(){return confirm("你确认拒绝吗？")}',
                ),
            )
        ),
    ),
)); ?>
