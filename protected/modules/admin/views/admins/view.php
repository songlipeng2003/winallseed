<?php
$this->pageTitle = Yii::t('models', 'models.Admin') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
	Yii::t('models', 'models.Admin')=>array('index'),
	$model->id,
);
?>

<h1>查看<?php echo Yii::t('models', 'models.Admin') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		array(
            'name'=>'adminGroupId',
            'value'=>$model->adminGroup->name
        ),
		'username',
		'realname',
		array(
			'name'=>'gender',
			'value'=>Constants::$genders[$model->gender]
		),
		'mobile',
		'address',
		array(
            'name'=>'status',
            'value'=>Admin::$statuses[$model->status]
        ),
		'createTime',
		'updateTime',
),
)); ?>
