<?php
$this->pageTitle = Yii::t('models', 'models.Admin') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
	Yii::t('models', 'models.Admin')=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'更新',
);
?>

<h1>更新<?php echo Yii::t('models', 'models.Admin') ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>