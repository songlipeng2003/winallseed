<?php
$this->pageTitle = Yii::t('models', 'models.Admin') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
    Yii::t('models', 'models.Admin')=>array('index'),
    '管理',
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('admin-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<h1><?php echo Yii::t('models', 'models.Admin') ?>管理</h1>

<?php echo CHtml::link('<i class="glyphicon glyphicon-plus"></i>创建'.Yii::t('models', 'models.Admin'), array('create'), array('class'=>'btn btn-primary')) ?><br/>
<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'admin-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        array(
            'name'=>'adminGroupId',
            'value'=>'$data->adminGroup->name',
            'filter'=>CHtml::activeDropDownList($model, 'adminGroupId', CHtml::listData(AdminGroup::model()->findAll(), 'id', 'name'), array(
                'class'=>'form-control',
                'empty'=>'请选择'
            )),
            'htmlOptions'=>array('width'=>120),
        ),
		'username',
		'realname',
        'mobile',
        array(
            'name'=>'status',
            'value'=>'Admin::$statuses[$data->status]',
            'filter'=>CHtml::activeDropDownList($model, 'status', Admin::$statuses, array(
                'class'=>'form-control',
                'empty'=>'请选择'
            )),
            'htmlOptions'=>array('width'=>100),
        ),
		'createTime',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
        ),
    ),
)); ?>
