<?php
$this->pageTitle = Yii::t('models', 'models.Group') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
	Yii::t('models', 'models.Group')=>array('index'),
	$model->name,
);
?>

<h1>查看<?php echo Yii::t('models', 'models.Group') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name',
        array(
            'name'=>'menu',
            'type'=>'raw',
            'value'=>nl2br($model->menuText)
        ),
		'createTime',
		'updateTime',
),
)); ?>
