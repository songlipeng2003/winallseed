<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'group-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true
	),
)); ?>

<p class="help-block">带 <span class="required">*</span> 必填.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->checkboxListGroup($model,'menu',array(
        'widgetOptions'=>array(
            'htmlOptions'=>array(
                'class'=>'span5',
                'maxlength'=>255
            ),
            'data'=>$auths
        )
    )); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? '创建' : '更新',
		)); ?>
</div>

<?php $this->endWidget(); ?>
