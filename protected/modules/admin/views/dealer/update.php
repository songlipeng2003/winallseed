<?php
$this->pageTitle = Yii::t('models', 'models.Dealer') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
	Yii::t('models', 'models.Dealer')=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'更新',
);
?>

<h1>更新<?php echo Yii::t('models', 'models.Dealer') ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>