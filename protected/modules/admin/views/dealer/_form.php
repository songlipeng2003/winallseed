<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'dealer-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true
	),
)); ?>

<p class="help-block">带 <span class="required">*</span> 必填.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'companyName',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'contact',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'mobile',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'phone',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'provinceId',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255,
                'empty'=>'请选择'
			),
			'data'=>CHtml::listData(Area::model()->findAllTop(), 'id', 'name')
		),
	)); ?>

	<?php echo $form->dropDownListGroup($model,'cityId',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255,
                'empty'=>'请选择'
			),
			'data'=>CHtml::listData(Area::model()->findAllByParent($model->provinceId), 'id', 'name'),
		),
	)); ?>

	<?php echo $form->dropDownListGroup($model,'areaId',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255,
                'empty'=>'请选择'
			),
			'data'=>CHtml::listData(Area::model()->findAllByParent($model->cityId), 'id', 'name')
		),
	)); ?>

	<?php echo $form->textFieldGroup($model,'address',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'product',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'status',array(
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255
			),
			'data'=>Dealer::$statuses
		)
	)); ?>

	<?php echo $form->hiddenField($model,'longitude'); ?>

	<?php echo $form->hiddenField($model,'latitude'); ?>

	<div class="form-group">
		<label for="" class="col-sm-3 control-label"></label>
		<div class="col-sm-9">
			<div class="col-sm-5">
				<input type="address" class="form-control" id="address" placeholder="请输入详细地址">
			</div>
			<button id="search" type="submit" class="btn btn-default">搜索</button>
			<div id="map" style="width:100%;height:500px;"></div>
		</div>
	</div>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? '创建' : '更新',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=oMn7nlpF0clkAqEKII0Lvs9R"></script>
<script type="text/javascript">
$(function(){
	var map = new BMap.Map("map");
	<?php if($model->longitude){ ?>
	var point = new BMap.Point(<?php echo $model->longitude; ?>, <?php echo $model->latitude; ?>);
	<?php }else{ ?>
	var point = new BMap.Point(117.282699, 31.866942);
	<?php } ?>
	map.centerAndZoom(point,15);
	map.addControl(new BMap.MapTypeControl());
	map.addControl(new BMap.NavigationControl());
	var marker = new BMap.Marker(point);
	marker.enableDragging(true);
	marker.addEventListener('dragend', moveMarker);
	map.addOverlay(marker);

	$('#search').click(function(){
		var address = $('#address').val();

		var myGeo = new BMap.Geocoder();
		myGeo.getPoint(address, function(point){
			$('#Dealer_longitude').val(point.lng);
			$('#Dealer_latitude').val(point.lat);

			if (point) {
				map.centerAndZoom(point, 16);
				marker.remove();
				marker = new BMap.Marker(point);
				marker.enableDragging(true);
				marker.addEventListener('dragend', moveMarker);
				map.addOverlay(marker);
			}
		}, "全国");

		return false;
	});

	area_select($('#Dealer_provinceId'), $('#Dealer_cityId'), $('#Dealer_areaId'));
});

function moveMarker(e){
	$('#Dealer_longitude').val(e.point.lng);
	$('#Dealer_latitude').val(e.point.lat);
}
</script>