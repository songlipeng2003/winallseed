<?php
$this->pageTitle = Yii::t('models', 'models.Dealer') . '管理-' . $this->pageTitle;$this->breadcrumbs=array(
	Yii::t('models', 'models.Dealer')=>array('index'),
	$model->name,
);

$this->menu=array(
    array('label'=>'管理','url'=>array('index')),
    array('label'=>'创建','url'=>array('create')),
    array('label'=>'更新','url'=>array('update','id'=>$model->id)),
    array('label'=>'删除','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'你确定要删除吗?')),
);
?>

<h1>查看<?php echo Yii::t('models', 'models.Dealer') ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
        array(
            'name'=>'admin',
            'value'=>$model->admin ? $model->admin->username : ""
        ),
		'name',
		'companyName',
		'contact',
		'mobile',
		'phone',
        array(
            'name'=>'provinceId',
            'value'=>$model->province->name,
        ),
        array(
            'name'=>'cityId',
            'value'=>$model->city->name,
        ),
        array(
            'name'=>'areaId',
            'value'=>$model->area->name,
        ),
		'address',
		'product',
		array(
            'name'=>'status',
            'value'=>Dealer::$statuses[$model->status]
        ),
		'longitude',
		'latitude',
		'createTime',
		'updateTime',
),
)); ?>
