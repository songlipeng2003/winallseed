<?php

class BaseSettingForm extends CFormModel
{
    public function save()
    {
        Yii::app()->settings->set('system', $this->attributes);

        return true;
    }
}