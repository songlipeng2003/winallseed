<?php

class ReplySettingForm extends BaseSettingForm
{
    public $defaultReply;
    public $welcomeReply;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('defaultReply, welcomeReply', 'length', 'max'=>32),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'defaultReply'=>'默认信息关键字',
            'welcomeReply'=>'欢迎信息关键字',
        );
    }
}