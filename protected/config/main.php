<?php
Yii::setPathOfAlias('vendor', dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../vendor');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'荃银微信服务平台',
	'language'=>'zh_cn',

	// preloading 'log' component
	'preload'=>array(
		'log',
		'booster'
	),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'vendor.zhdanovartur.yii-easyimage.EasyImage',
	),

	'modules'=>array(
		'admin',
		'weixin',
		'user',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'demo',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths'=>array(
                'application.gii',
            ),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		'adminUser'=>array(
			'class' => 'AdminWebUser',
			'allowAutoLogin'=>true,
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=testdrive',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'enableProfiling'=>true,
        	'enableParamLogging'=>true,
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
	            array(
	                'class'=>'vendor.malyshev.yii-debug-toolbar.YiiDebugToolbarRoute',
	                'ipFilters'=>array('127.0.0.1'),
	            ),
			),
		),
		'booster' => array(
		    'class' => 'vendor.clevertech.yii-booster.src.components.Booster',
		    'fontAwesomeCss' => true,
		),
		'easyImage' => array(
			'class' => 'vendor.zhdanovartur.yii-easyimage.EasyImage',
			//'driver' => 'GD',
			//'quality' => 100,
			//'cachePath' => '/assets/easyimage/',
			//'cacheTime' => 2592000,
			//'retinaSupport' => false,
		),
		'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),
		'settings'=>array(
	        'class'             => 'application.extensions.settings.CmsSettings',
	        'cacheComponentId'  => 'cache',
	        'cacheId'           => 'global_website_settings',
	        'cacheTime'         => 84000,
	        'tableName'         => 'settings',
	        'dbComponentId'     => 'db',
	        'createTable'       => true,
	        'dbEngine'          => 'InnoDB',
        ),
	),
	'controllerMap'=>array(
        'ueditor'=>array(
            'class'=>'ext.ueditor.UeditorController',
            'config'=>array(),//参考config.json的配置，此处的配置具备最高优先级
            'thumbnail'=>true,//是否开启缩略图
            'watermark'=>'',//水印图片的地址，使用相对路径
            'locate'=>9,//水印位置，1-9，默认为9在右下角
        ),
    ),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'weixinToken'=>'0551xinxi',
		'weixinAppid'=>'wxdc6f9dde532c478b',
		'weixinppsecret'=>'e787aed9a5d7d92a693eee94e9b5707e',
	),
);