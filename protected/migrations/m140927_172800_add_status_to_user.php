<?php

class m140927_172800_add_status_to_user extends CDbMigration
{
	public function up()
	{
		$this->addColumn('user', 'status', 'tinyint NOT NULL DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('user', 'status');
		return true;
	}
}