<?php

class m140916_143603_create_dealer extends CDbMigration
{
	public function up()
	{
		$this->createTable('dealer', array(
			'id'=>'pk',
			'name'=>'string NOT NULL',
			'companyName'=>'string NOT NULL',
			'contact'=>'string NOT NULL',
			'mobile'=>'string NOT NULL',
			'phone'=>'string NOT NULL',
			'address'=>'string NOT NULL',
			'product'=>'string NOT NULL',
			'status'=>'tinyint NOT NULL DEFAULT 0',
			'longitude'=>'float NOT NULL',
			'latitude'=>'float NOT NULL',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));
	}

	public function down()
	{	
		$this->dropTable('dealer');

		return true;
	}
}