<?php

class m140924_134756_rename_admin_id_to_adminId extends CDbMigration
{
	public function up()
	{
		$this->renameColumn('dealer', 'admin_id', 'adminId');
	}

	public function down()
	{
		$this->renameColumn('dealer', 'adminId', 'admin_id');
		return false;
	}
}