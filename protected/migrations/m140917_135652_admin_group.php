<?php

class m140917_135652_admin_group extends CDbMigration
{
	public function up()
	{
		$this->createTable('admin_group', array(
			'id'=>'pk',
			'name'=>'string NOT NULL',
			'menu'=>'text NOT NULL',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));

		$this->createIndex('index_name', 'admin_group', 'name', true);
	}

	public function down()
	{
		$this->dropIndex('index_name', 'admin_group');
		
		$this->dropTable('admin_group');

		return true;
	}
}