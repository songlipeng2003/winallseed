<?php

class m141026_155435_create_article extends CDbMigration
{
	public function up()
	{
		$this->createTable('article', array(
			'id'=>'pk',
			'title'=>'string NOT NULL',
			'cover'=>'string NOT NULL',
			'description'=>'string NOT NULL',
			'content'=>'text NOT NULL',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('article');

		return true;
	}
}