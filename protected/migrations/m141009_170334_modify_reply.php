<?php

class m141009_170334_modify_reply extends CDbMigration
{
	public function up()
	{
		$this->addColumn('reply', 'type', 'tinyint NOT NULL DEFAULT 1');
		$this->addColumn('reply', 'keywords', 'string');
		$this->addColumn('reply', 'title', 'string');
		$this->addColumn('reply', 'cover', 'string');
		$this->addColumn('reply', 'description', 'string');
		$this->addColumn('reply', 'link', 'string');
	}

	public function down()
	{
		$this->dropColumn('reply', 'type');
		$this->dropColumn('reply', 'keywords');
		$this->dropColumn('reply', 'title');
		$this->dropColumn('reply', 'cover');
		$this->dropColumn('reply', 'description');
		$this->dropColumn('reply', 'link');
		return true;
	}
}