<?php

class m141014_162419_add_area_to_user extends CDbMigration
{
	public function up()
	{
		$this->addColumn('user', 'provinceId', 'int');
		$this->addColumn('user', 'cityId', 'int');
		$this->addColumn('user', 'areaId', 'int');
	}

	public function down()
	{
		$this->dropColumn('user', 'areaId');
		$this->dropColumn('user', 'cityId');
		$this->dropColumn('user', 'provinceId');
		return true;
	}
}