<?php

class m141006_121922_create_media extends CDbMigration
{
	public function up()
	{
		$this->createTable('media', array(
			'id'=>'pk',
			'type'=>'string NOT NULL',
			'media'=>'string NOT NULL',
			'filename'=>'string',
			'filelength'=>'string',
			'url'=>'string',
			'mediaId'=>'int',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('media');

		return true;
	}
}