<?php

class m141006_100851_create_menu extends CDbMigration
{
	public function up()
	{
		$this->createTable('menu', array(
			'id'=>'pk',
			'parentId'=>'int NOT NULL',
			'name'=>'string NOT NULL',
			'type'=>'string',
			'key'=>'string',
			'url'=>'string',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('menu');

		return true;
	}
}