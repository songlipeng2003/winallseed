<?php

class m140916_162719_group extends CDbMigration
{
	public function up()
	{
		$this->createTable('group', array(
			'id'=>'pk',
			'name'=>'string NOT NULL',
			'menu'=>'text NOT NULL',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));

		$this->createIndex('index_name', 'group', 'name', true);
	}

	public function down()
	{
		$this->dropIndex('index_name', 'group');
		
		$this->dropTable('group');

		return true;
	}
}