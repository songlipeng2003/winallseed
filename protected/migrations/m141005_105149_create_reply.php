<?php

class m141005_105149_create_reply extends CDbMigration
{
	public function up()
	{
		$this->createTable('reply', array(
			'id'=>'pk',
			'name'=>'string NOT NULL',
			'content'=>'text',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('reply');

		return true;
	}
}