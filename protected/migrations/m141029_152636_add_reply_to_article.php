<?php

class m141029_152636_add_reply_to_article extends CDbMigration
{
	public function up()
	{
		$this->addColumn('article', 'replyId', 'int');
	}

	public function down()
	{
		$this->dropColumn('article', 'replyId');
		return true;
	}
}