<?php

class m140917_142716_add_admin_id_to_dealer extends CDbMigration
{
	public function up()
	{
		$this->addColumn('dealer', 'admin_id', 'int NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('dealer', 'admin_id');
		return true;
	}
}