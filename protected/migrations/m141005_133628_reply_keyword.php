<?php

class m141005_133628_reply_keyword extends CDbMigration
{
	public function up()
	{
		$this->createTable('reply_keyword', array(
			'id'=>'pk',
			'replyId'=>'int NOT NULL',
			'keyword'=>'string NOT NULL',
			'createTime'=>'datetime NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('reply_keyword');

		return true;
	}
}