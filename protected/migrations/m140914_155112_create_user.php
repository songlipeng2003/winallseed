<?php

class m140914_155112_create_user extends CDbMigration
{
	public function up()
	{
		$this->createTable('user', array(
			'id'=>'pk',
			'username'=>'string NOT NULL',
			'password'=>'string NOT NULL',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('user');

		return true;
	}
}