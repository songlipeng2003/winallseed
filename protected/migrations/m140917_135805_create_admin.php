<?php

class m140917_135805_create_admin extends CDbMigration
{
	public function up()
	{
		$this->createTable('admin', array(
			'id'=>'pk',
			'username'=>'string NOT NULL',
			'password'=>'string NOT NULL',
			'realname'=>'string NOT NULL',
			'gender'=>'tinyint NOT NULL DEFAULT 0',
			'mobile'=>'string NOT NULL',
			'address'=>'string',
			'admin_group_id'=>'string NOT NULL',
			'status'=>'tinyint NOT NULL DEFAULT 0',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));

		$this->createIndex('index_username', 'admin', 'username', true);
	}

	public function down()
	{
		$this->dropIndex('index_username', 'admin');
		
		$this->dropTable('admin');

		return true;
	}
}