<?php

class m141014_170248_add_area_to_dealer extends CDbMigration
{
	public function up()
	{
		$this->addColumn('dealer', 'provinceId', 'int');
		$this->addColumn('dealer', 'cityId', 'int');
		$this->addColumn('dealer', 'areaId', 'int');
	}

	public function down()
	{
		$this->dropColumn('dealer', 'areaId');
		$this->dropColumn('dealer', 'cityId');
		$this->dropColumn('dealer', 'provinceId');
		return true;
	}
}