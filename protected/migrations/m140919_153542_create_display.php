<?php

class m140919_153542_create_display extends CDbMigration
{
	public function up()
	{
		$this->createTable('display', array(
			'id'=>'pk',
			'user_id'=>'int NOT NULL',
			'img'=>'string NOT NULL',
			'description'=>'text',
			'longitude'=>'float',
			'latitude'=>'float',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('display');

		return true;
	}
}