<?php

class m141012_153623_add_position_to_user extends CDbMigration
{
	public function up()
	{
		$this->addColumn('user', 'longitude', 'string');
		$this->addColumn('user', 'latitude', 'string');
	}

	public function down()
	{
		$this->dropColumn('user', 'longitude');
		$this->dropColumn('user', 'latitude');
		return true;
	}
}