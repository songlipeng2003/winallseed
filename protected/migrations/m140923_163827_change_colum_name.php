<?php

class m140923_163827_change_colum_name extends CDbMigration
{
	public function up()
	{
		$this->renameColumn('admin', 'admin_group_id', 'adminGroupId');
		$this->renameColumn('user', 'group_id', 'groupId');
	}

	public function down()
	{
		$this->renameColumn('admin', 'adminGroupId', 'admin_group_id');
		$this->renameColumn('user', 'groupId', 'group_id');
		return true;
	}
}