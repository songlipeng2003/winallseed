<?php

class m141019_132316_add_status_to_display extends CDbMigration
{
	public function up()
	{
		$this->addColumn('display', 'status', 'tinyint NOT NULL DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('display', 'status');
		return false;
	}
}