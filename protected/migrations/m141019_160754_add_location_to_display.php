<?php

class m141019_160754_add_location_to_display extends CDbMigration
{
	public function up()
	{
		$this->addColumn('display', 'location', 'string');
	}

	public function down()
	{
		$this->dropColumn('display', 'location');
		return false;
	}
}