<?php

class m140917_154710_add_group_id_to_user extends CDbMigration
{
	public function up()
	{
		$this->addColumn('user', 'group_id', 'int NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('user', 'group_id');
		return true;
	}
}