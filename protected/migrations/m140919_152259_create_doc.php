<?php

class m140919_152259_create_doc extends CDbMigration
{
	public function up()
	{
		$this->createTable('doc', array(
			'id'=>'pk',
			'title'=>'string NOT NULL',
			'content'=>'text NOT NULL',
			'createTime'=>'datetime NOT NULL',
			'updateTime'=>'datetime NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('doc');

		return true;
	}
}