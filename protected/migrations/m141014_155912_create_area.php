<?php

class m141014_155912_create_area extends CDbMigration
{
	public function up()
	{
		$this->createTable('area', array(
			'id'=>'pk',
			'name'=>'string NOT NULL',
			'parentId'=>'int',
			'level'=>'tinyint'
		));
	}

	public function down()
	{
		$this->dropTable('area');

		return true;
	}
}