<?php

class m140916_161814_modify_user extends CDbMigration
{
	public function up()
	{
		$this->addColumn('user', 'openid', 'string NOT NULL');
		$this->addColumn('user', 'avatar', 'string');
		$this->addColumn('user', 'nickname', 'string');
		$this->addColumn('user', 'gender', 'tinyint NOT NULL DEFAULT 0');
		$this->addColumn('user', 'mobile', 'string');
		$this->addColumn('user', 'phone', 'string');
		$this->addColumn('user', 'address', 'string');
		$this->addColumn('user', 'note', 'string');
	}

	public function down()
	{
		$this->dropColumn('user', 'openid');
		$this->dropColumn('user', 'avatar');
		$this->dropColumn('user', 'nickname');
		$this->dropColumn('user', 'gender');
		$this->dropColumn('user', 'mobile');
		$this->dropColumn('user', 'phone');
		$this->dropColumn('user', 'address');
		$this->dropColumn('user', 'note');

		return true;
	}
}