<?php

/**
 * This is the model class for table "article".
 *
 * The followings are the available columns in table 'article':
 * @property integer $id
 * @property string $title
 * @property string $cover
 * @property string $description
 * @property string $content
 * @property string $createTime
 * @property string $updateTime
 */
class Article extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'article';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, content, cover', 'required'),
			array('title', 'length', 'max'=>32),
			array('cover, description', 'length', 'max'=>255),
			//array('cover', 'file', 'types'=>array('jpg', 'jpeg', 'png', 'gif'), 'allowEmpty'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, cover, description, content, createTime, updateTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reply'=>array(self::BELONGS_TO, 'Reply', 'replyId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '编号',
			'title' => '标题',
			'cover' => '封面',
			'description' => '描述',
			'content' => '内容',
			'createTime' => '创建时间',
			'updateTime' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('replyId',$this->replyId,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('cover',$this->cover,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('createTime',$this->createTime,true);
		$criteria->compare('updateTime',$this->updateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		    'sort'=>array(
		        'defaultOrder'=>'id DESC',
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'createTime',
				'updateAttribute' => 'updateTime',
			)
		);
	}

	protected function beforeSave()
	{
		parent::beforeSave();

		if(strpos($this->cover, '/upload/tmp/')===0)
		{
			if(!file_exists(Yii::getPathOfAlias('webroot').$this->cover))
			{
				$this->addError('cover', '封面图片不存在');
				return false;
			}

			$filePath = '/upload/article/' . strftime('%Y-%m-%d') . '/';
			$ext = pathinfo($this->cover, PATHINFO_EXTENSION);
	        $fileName = uniqid() . '.' . $ext;
	        @ mkdir(Yii::getPathOfAlias('webroot').$filePath, 0777, true);
	        rename(Yii::getPathOfAlias('webroot').$this->cover, Yii::getPathOfAlias('webroot').$filePath.$fileName);
	        $this->cover = $filePath.$fileName;
		}

		return true;
	}
}
