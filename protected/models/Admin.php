<?php

/**
 * 管理员
 *
 * The followings are the available columns in table 'admin':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $realname
 * @property integer $gender
 * @property string $mobile
 * @property string $address
 * @property string $adminGroupId
 * @property integer $status
 * @property string $createTime
 * @property string $updateTime
 */
class Admin extends CActiveRecord
{
	const STATUS_NORMAL = 0;
	const STATUS_HIDDEN = 1;

	static $statuses = array(
		self::STATUS_NORMAL => '有效',
		self::STATUS_HIDDEN => '无效',
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, realname, mobile, adminGroupId', 'required'),
			array('username', 'unique'),
			array('password', 'required', 'on'=>'create'),
			array('gender, status', 'numerical', 'integerOnly'=>true),
			array('username, password, realname, mobile, address', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, realname, gender, mobile, address, adminGroupId, status, createTime, updateTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'adminGroup'=>array(self::BELONGS_TO, 'AdminGroup', 'adminGroupId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '编码',
			'username' => '用户名',
			'password' => '密码',
			'realname' => '真实姓名',
			'gender' => '性别',
			'mobile' => '手机',
			'address' => '地址',
			'adminGroupId' => '管理员分组',
			'status' => '状态',
			'createTime' => '创建时间',
			'updateTime' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('realname',$this->realname,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('adminGroupId',$this->adminGroupId,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('createTime',$this->createTime,true);
		$criteria->compare('updateTime',$this->updateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		    'sort'=>array(
		        'defaultOrder'=>'id DESC',
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Admin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'createTime',
				'updateAttribute' => 'updateTime',
			)
		);
	}

	protected function beforeSave()
	{
		// if(sizeof($this->password)!=32)
		// {
		// 	$this->password = md5($this->password);
		// }

		return parent::beforeSave();
	}

	public function login($username, $password)
	{
		// $password = md5($password);
		return $this->findByAttributes(array('username'=>$username, 'password'=>$password));
	}
}
