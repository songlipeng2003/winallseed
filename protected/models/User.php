<?php

/**
 * 用户
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $createTime
 * @property string $updateTime
 * @property string $openid
 * @property string $avatar
 * @property string $nickname
 * @property integer $gender
 * @property string $mobile
 * @property string $phone
 * @property string $address
 * @property string $note
 * @property integer $status
 */
class User extends CActiveRecord
{
	const STATUS_DEFAULT = 0;
	const STATUS_APPLY = 1;
	const STATUS_NORMAL = 2;
	const STATUS_REFUSE = 3;

	static $statuses = array(
		self::STATUS_APPLY => '申请中',
		self::STATUS_NORMAL => '已审核',
		self::STATUS_REFUSE => '已拒绝'
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('openid', 'required'),
			array('username, gender, mobile, phone, address, provinceId, cityId, areaId', 'required', 'on'=>'register'),
			array('password', 'length', 'min'=>6, 'max'=>32, 'on'=>'register'),
			array('mobile', 'length', 'is'=>11, 'on'=>'register'),
			array('gender, status, groupId, provinceId, cityId, areaId', 'numerical', 'integerOnly'=>true),
			array('username, nickname, mobile, phone, address', 'length', 'max'=>32),
			array('avatar', 'length', 'max'=>256),
			array('note', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, createTime, updateTime, openid, avatar, nickname, gender, mobile, phone, address, note', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'group'=>array(self::BELONGS_TO, 'Group', 'groupId'),
			'province'=>array(self::BELONGS_TO, 'Area', 'provinceId'),
			'city'=>array(self::BELONGS_TO, 'Area', 'cityId'),
			'area'=>array(self::BELONGS_TO, 'Area', 'areaId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '编号',
			'groupId' => '群组',
			'username' => '用户名',
			'password' => '密码',
			'openid' => 'Openid',
			'avatar' => '头像',
			'nickname' => '昵称',
			'gender' => '性别',
			'mobile' => '手机',
			'phone' => '电话',
			'provinceId' => '省份',
			'cityId' => '城市',
			'areaId' => '区县',
			'address' => '地址',
			'note' => '备注',
			'status' => '状态',
			'createTime' => '创建时间',
			'updateTime' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('createTime',$this->createTime,true);
		$criteria->compare('updateTime',$this->updateTime,true);
		$criteria->compare('openid',$this->openid,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('nickname',$this->nickname,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('note',$this->note,true);
		if(!$this->status){
			$criteria->addInCondition('status', array_keys(self::$statuses));
		}else{
			$criteria->compare('status',$this->status,true);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		    'sort'=>array(
		        'defaultOrder'=>'id DESC',
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'createTime',
				'updateAttribute' => 'updateTime',
			)
		);
	}

	public function findByOpenid($openid){
		return $this->findByAttributes(array('openid'=>$openid));
	}

	public function login($username, $password)
	{
		$password = md5($password);
		return $this->findByAttributes(array('username'=>$username, 'password'=>$password));
	}

	public function confirm()
	{
		$this->status = self::STATUS_NORMAL;
		return $this->save();
	}

	public function refuse()
	{
		$this->status = self::STATUS_REFUSE;
		return $this->save();
	}

	public function getAvatarUrl($size=132)
	{
		if($this->avatar){
			$avatar = substr($this->avatar, sizeof($this->avatar)-1) . $size;
			return $avatar;
		}

		return false;
	}
}
