<?php

/**
 * 经销商
 *
 * The followings are the available columns in table 'dealer':
 * @property integer $id
 * @property string $name
 * @property string $companyName
 * @property string $contact
 * @property string $mobile
 * @property string $phone
 * @property string $address
 * @property string $product
 * @property integer $status
 * @property double $longitude
 * @property double $latitude
 * @property string $createTime
 * @property string $updateTime
 */
class Dealer extends CActiveRecord
{
	const STATUS_NORMAL = 0;
	const STATUS_HIDDEN = 1;

	static $statuses = array(
		self::STATUS_NORMAL => '正常',
		self::STATUS_HIDDEN => '隐藏',
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dealer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, companyName, contact, mobile, phone, address, product, status, longitude, latitude, provinceId, cityId, areaId', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('longitude, latitude, provinceId, cityId, areaId', 'numerical'),
			array('name, companyName, contact, mobile, phone, address, product', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, companyName, contact, mobile, phone, address, product, status, longitude, latitude, createTime, updateTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'admin'=>array(self::BELONGS_TO, 'Admin', 'adminId'),
			'province'=>array(self::BELONGS_TO, 'Area', 'provinceId'),
			'city'=>array(self::BELONGS_TO, 'Area', 'cityId'),
			'area'=>array(self::BELONGS_TO, 'Area', 'areaId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '编号',
			'admin' => '管理员',
			'name' => '门店名称',
			'companyName' => '公司名称',
			'contact' => '联系人',
			'mobile' => '手机',
			'phone' => '电话',
			'provinceId' => '省份',
			'cityId' => '城市',
			'areaId' => '区县',
			'address' => '地址',
			'product' => '产品',
			'status' => '状态',
			'longitude' => '经度',
			'latitude' => '纬度',
			'createTime' => '创建时间',
			'updateTime' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('companyName',$this->companyName,true);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('product',$this->product,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('createTime',$this->createTime,true);
		$criteria->compare('updateTime',$this->updateTime,true);

		$criteria->compare('provinceId',$this->provinceId,true);
		$criteria->compare('cityId',$this->city,true);
		$criteria->compare('areaId',$this->areaId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		    'sort'=>array(
		        'defaultOrder'=>'id DESC',
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dealer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'createTime',
				'updateAttribute' => 'updateTime',
			)
		);
	}

	public function findByPoint($latitude, $longitude)
	{
		$sql = "SELECT *, SQRT(POWER($latitude - latitude, 2) + POWER($longitude  - longitude, 2)) AS d 
			FROM dealer 
			WHERE 1=1
			ORDER BY d ASC 
			LIMIT 10;";
		return $this->findAllBySql($sql);
	}

	public function findByProduct($product, $latitude=null, $longitude=null)
	{
		if($latitude){
			$sql = "SELECT *, SQRT(POWER($latitude - latitude, 2) + POWER($longitude  - longitude, 2)) AS d 
				FROM dealer 
				WHERE product LIKE '%{$product}%'
				ORDER BY d ASC 
				LIMIT 10;";
		}else{
			$sql = "SELECT * FROM dealer WHERE product LIKE '%{$product}%' ORDER BY id DESC LIMIT 10;";
		}
		
		return $this->findAllBySql($sql);
	}

	public function findByAreaAndKeyword($city, $area, $product)
	{
		$city = Area::model()->findByKeywordAndLevel($city, 2);
		if($city)
		{
			$area = Area::model()->findByKeywordAndParent($area, $city->id);
			if($area)
			{
				$criteria=new CDbCriteria;

				$criteria->addCondition("areaId={$area->id} AND product LIKE '%$product%'");
				$criteria->limit = '10';

				return $this->findAll($criteria);
			}
		}

		return array();
	}
}
