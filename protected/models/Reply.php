<?php

/**
 * 自动回复
 *
 * The followings are the available columns in table 'reply':
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property string $createTime
 * @property string $updateTime
 * @property integer $type
 * @property string $keywords
 * @property string $title
 * @property string $cover
 * @property string $description
 * @property string $link
 */
class Reply extends CActiveRecord
{
	const TYPE_TEXT = 1;
	const TYPE_NEWS = 2;
	const TYPE_MUSIC = 3;

	public static $types = array(
		self::TYPE_TEXT=>'文字',
		self::TYPE_NEWS=>'图文'
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reply';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, keywords, content', 'required'),
			array('cover, title, description', 'required', 'on'=>'article'),
			array('type', 'numerical', 'integerOnly'=>true),
			array('name, keywords, title, cover, description, link', 'length', 'max'=>255),
			array('content, cover', 'safe'),
			// array('cover', 'file', 'types'=>array('jpg', 'jpeg', 'png', 'gif'), 'allowEmpty'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, content, createTime, updateTime, type, keywords, title, cover, description, link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'articles'=>array(self::HAS_MANY, 'Article', 'replyId', 'order'=>'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '编号',
			'name' => '名称',
			'content' => '内容',
			'createTime' => '创建时间',
			'updateTime' => '更新时间',
			'type' => '类型',
			'keywords' => '关键词',
			'title' => '标题',
			'cover' => '封面',
			'description' => '描述',
			'link' => '链接',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('createTime',$this->createTime,true);
		$criteria->compare('updateTime',$this->updateTime,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('cover',$this->cover,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('link',$this->link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		    'sort'=>array(
		        'defaultOrder'=>'id DESC',
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reply the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave()
	{
		parent::beforeSave();

		if(strpos($this->cover, '/upload/tmp/')===0)
		{
			if(!file_exists(Yii::getPathOfAlias('webroot').$this->cover))
			{
				$this->addError('cover', '封面图片不存在');
				return false;
			}

			$filePath = '/upload/article/' . strftime('%Y-%m-%d') . '/';
			$ext = pathinfo($this->cover, PATHINFO_EXTENSION);
	        $fileName = uniqid() . '.' . $ext;
	        @ mkdir(Yii::getPathOfAlias('webroot').$filePath, 0777, true);
	        $image = new EasyImage(Yii::getPathOfAlias('webroot').$this->cover);
			$image->resize(360, 200);
			$image->save(Yii::getPathOfAlias('webroot').$filePath.$fileName);
	        $this->cover = $filePath.$fileName;
		}

		return true;
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'createTime',
				'updateAttribute' => 'updateTime',
			)
		);
	}

	public function findByKeyword($keyword)
	{
		return $this->findByAttributes(array('keywords'=>$keyword));
	}
}
