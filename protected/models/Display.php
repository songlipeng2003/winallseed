<?php

/**
 * 示范展示
 *
 * The followings are the available columns in table 'display':
 * @property integer $id
 * @property integer $user_id
 * @property string $img
 * @property string $description
 * @property double $longitude
 * @property double $latitude
 * @property string $createTime
 * @property string $updateTime
 */
class Display extends CActiveRecord
{
	const STATUS_APPLY = 0;
	const STATUS_NORMAL = 1;
	const STATUS_REFUSE = 2;

	static $statuses = array(
		self::STATUS_APPLY => '待审核',
		self::STATUS_NORMAL => '已审核',
		self::STATUS_REFUSE => '已拒绝'
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'display';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, img', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('longitude, latitude, status', 'numerical'),
			array('img, location', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, img, description, longitude, latitude, createTime, updateTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '编号',
			'user_id' => '用户',
			'img' => '图片',
			'description' => '描述',
			'longitude' => '经度',
			'latitude' => '纬度',
			'status' => '状态',
			'location' => '位置',
			'createTime' => '创建时间',
			'updateTime' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('createTime',$this->createTime,true);
		$criteria->compare('updateTime',$this->updateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		    'sort'=>array(
		        'defaultOrder'=>'id DESC',
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Display the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'createTime',
				'updateAttribute' => 'updateTime',
			)
		);
	}

	public function confirm()
	{
		$this->status = self::STATUS_NORMAL;
		return $this->save();
	}

	public function refuse()
	{
		$this->status = self::STATUS_REFUSE;
		return $this->save();
	}
}
